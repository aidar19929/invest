package kz.investgroup.invest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@SpringBootApplication
public class InvestApplication {

  public static void main(String[] args) {
    SpringApplication.run(InvestApplication.class, args);
  }

}
