package kz.investgroup.invest.config;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import kz.investgroup.invest.config.api.ApiError;
import kz.investgroup.invest.config.api.ApiSubError;
import kz.investgroup.invest.config.api.ApiValidationError;
import kz.investgroup.invest.security.UnauthorizedException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler(Exception.class)
  public ResponseEntity<Object> handleInternal(Exception ex, WebRequest request) {
    String error = "Internal error";
    log.error(error, ex);
    return buildResponseEntity(new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, error, ex));
  }

  @ExceptionHandler(AccessDeniedException.class)
  public ResponseEntity<Object> handleUnauthorized(AccessDeniedException ex, WebRequest request) {
    String error = "Access denied!";
    return buildResponseEntity(new ApiError(HttpStatus.FORBIDDEN, error, ex));
  }

  @ExceptionHandler(UnauthorizedException.class)
  public ResponseEntity<Object> handleUnauthorized(UnauthorizedException ex, WebRequest request) {
    String error = "Unauthorized";
    return buildResponseEntity(new ApiError(HttpStatus.UNAUTHORIZED, error, ex.getAuthException()));
  }

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                HttpHeaders headers, HttpStatus status, WebRequest request) {

    List<ApiSubError> validationErrors = ex.getBindingResult().getAllErrors()
        .stream()
        .map((err) -> {
          FieldError fieldErr = (FieldError) err;
          return ApiValidationError.builder()
              .field(fieldErr.getField())
              .rejectedValue(fieldErr.getRejectedValue())
              .message(fieldErr.getDefaultMessage())
              .build();
        }).collect(Collectors.toList());

    return buildResponseEntity(
        ApiError.builder()
            .timestamp(LocalDateTime.now())
            .status(HttpStatus.BAD_REQUEST)
            .subErrors(validationErrors)
            .message("Bad request!")
            .build()
    );
  }

  private ResponseEntity<Object> buildResponseEntity(ApiError apiError) {
    return new ResponseEntity<>(apiError, apiError.getStatus());
  }

}
