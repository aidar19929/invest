package kz.investgroup.invest.config;

import java.util.Arrays;
import kz.investgroup.invest.repository.AccountRepository;
import kz.investgroup.invest.repository.RoleRepository;
import kz.investgroup.invest.security.service.JwtUserDetailsDevService;
import kz.investgroup.invest.security.service.JwtUserDetailsService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
@RequiredArgsConstructor
public class UserDetailsServiceConfig {

  private final Environment env;
  private final RoleRepository roleRepository;
  private final AccountRepository accountRepository;

  @Bean
  public JwtUserDetailsService jwtUserDetailsService() {
    boolean isDev = Arrays.asList(env.getActiveProfiles()).contains("dev");
    return isDev ? new JwtUserDetailsDevService(roleRepository, accountRepository)
        : new JwtUserDetailsService(accountRepository);
  }

}
