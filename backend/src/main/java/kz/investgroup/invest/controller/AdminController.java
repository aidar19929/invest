package kz.investgroup.invest.controller;

import javax.validation.Valid;
import kz.investgroup.invest.model.account.AccountCreateRequest;
import kz.investgroup.invest.model.account.AccountCreateResponse;
import kz.investgroup.invest.service.AdminService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/admin")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class AdminController {

  private final AdminService service;

  @GetMapping
  public ResponseEntity<String> ok() {
    return ResponseEntity.ok("OK!");
  }

  @PostMapping("/account")
  public ResponseEntity<AccountCreateResponse> createAccount(@Valid  @RequestBody AccountCreateRequest request) {
    return ResponseEntity.ok(service.createAccount(request));
  }

  @PutMapping("/account/{accountId}/block")
  public void blockAccount(@PathVariable Long accountId) {
    service.blockAccount(accountId);
  }

  @PutMapping("/account/{accountId}/unblock")
  public void unblockAccount(@PathVariable Long accountId) {
    service.unblockAccount(accountId);
  }

}
