package kz.investgroup.invest.controller;

import kz.investgroup.invest.security.JwtRequest;
import kz.investgroup.invest.security.JwtResponse;
import kz.investgroup.invest.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/auth")
public class AuthController {

  private final AuthService authService;

  @PostMapping(value = "/login")
  public JwtResponse getToken(@RequestBody JwtRequest request) {
    return authService.getToken(request);
  }

  @PostMapping(value = "/token/refresh")
  public JwtResponse refreshToken(@RequestBody JwtRequest request) {
    return authService.refreshToken(request.getRefreshToken());
  }

}
