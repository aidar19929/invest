package kz.investgroup.invest.controller;

import kz.investgroup.invest.security.UnauthorizedException;
import kz.investgroup.invest.service.ClientAttributesHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/dummy")
@RequiredArgsConstructor
public class DummyController {

  private final ClientAttributesHelper helper;

  @GetMapping
  public ResponseEntity<String> hello() {
    return ResponseEntity.ok("Hello!");
  }

  @GetMapping(value = "/context")
  public ResponseEntity<Object> lang() {
    return ResponseEntity.ok(helper.getRequestContext());
  }

  @GetMapping(value = "/error")
  public ResponseEntity<String> error() {
    throw new RuntimeException("Dummy exception!");
  }

  @GetMapping(value = "/unauthorized")
  public ResponseEntity<String> unauthorized() {
    throw new UnauthorizedException(new InsufficientAuthenticationException("Bad credentials"));
  }

}
