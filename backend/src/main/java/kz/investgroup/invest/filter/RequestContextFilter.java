package kz.investgroup.invest.filter;

import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kz.investgroup.invest.model.LangEnum;
import lombok.Builder;
import lombok.Getter;
import org.springframework.web.servlet.HandlerInterceptor;

public class RequestContextFilter implements HandlerInterceptor {

  public static final ThreadLocal<RequestContext> CONTEXT = new ThreadLocal<>();

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
    LangEnum lang = LangEnum.parseOrNull(request.getHeader("Accept-Language"));

    RequestContext context = RequestContext.builder()
        .requestId(UUID.randomUUID().toString())
        .lang(lang == null ? LangEnum.RU : lang)
        .build();

    CONTEXT.set(context);
    return true;
  }

  @Override
  public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
    CONTEXT.remove();
  }

  @Getter
  @Builder
  public static class RequestContext {

    private final String requestId;
    private final LangEnum lang;

  }

}
