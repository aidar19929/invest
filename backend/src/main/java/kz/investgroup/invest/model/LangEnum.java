package kz.investgroup.invest.model;

import org.apache.logging.log4j.util.Strings;

public enum LangEnum {
  RU, EN, KK;

  public static LangEnum parseOrNull(String value) {
    if (Strings.isBlank(value)) {
      return null;
    }

    try {
      return LangEnum.valueOf(value.toUpperCase());
    } catch (Exception e) {
      return null;
    }
  }
}
