package kz.investgroup.invest.model;

import org.apache.logging.log4j.util.Strings;

public enum RoleEnum {
  ROLE_SYSTEM,
  ROLE_ADMIN,
  ROLE_MODERATOR,
  ROLE_INVESTOR,
  ROLE_BASIC,
  ROLE_LEVEL_LOCAL,
  ROLE_LEVEL_CENTRAL,
  ROLE_LEVEL_AUTHORISED,
  ROLE_LEVEL_COMMISSION;

  public static RoleEnum parseOrNull(String value) {
    if (Strings.isBlank(value)) {
      return null;
    }

    try {
      return RoleEnum.valueOf(value.toUpperCase());
    } catch (Exception e) {
      return null;
    }
  }
}
