package kz.investgroup.invest.model.account;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import kz.investgroup.invest.validation.Patronymic;

public class AccountCreateRequest {

  @NotEmpty(message = "{validation.email.notempty}")
  @Email
  public String username;

  @NotEmpty()
  @Size(min = 2, message = "{validation.value.moreThan.chars}")
  public String surname;

  @NotEmpty
  @Size(min = 2)
  public String name;

  @Patronymic(min = 2)
  public String patronymic;

  public Long companyId;

}
