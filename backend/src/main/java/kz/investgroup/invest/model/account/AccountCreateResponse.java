package kz.investgroup.invest.model.account;

public class AccountCreateResponse {

  public Long id;
  public String username;
  public String surname;
  public String name;
  public String patronymic;
  public Long companyId;

}
