package kz.investgroup.invest.model.entity;

import static javax.persistence.CascadeType.DETACH;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import kz.investgroup.invest.model.LangEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@Entity(name = "company")
@EqualsAndHashCode(callSuper = true)
public class Company extends AuditEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ToString.Exclude
  @MapKey(name = "localizedId.lang")
  @OneToMany(mappedBy = "company", cascade = {DETACH, MERGE, PERSIST, REFRESH}, orphanRemoval = true)
  private Map<LangEnum, CompanyLocalization> localizations = new HashMap<>();

  @ToString.Exclude
  @OneToMany(mappedBy = "company")
  private List<UserInfo> users = new ArrayList<>();

  public String getName(LangEnum lang) {
    CompanyLocalization local = localizations.get(lang);
    return local == null ? null : local.getName();
  }

}
