package kz.investgroup.invest.model.entity;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import kz.investgroup.invest.model.LangEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Embeddable
@NoArgsConstructor
@EqualsAndHashCode
public class LocalizedId implements Serializable {

  private static final long serialVersionUID = 1089196571270403924L;

  private Long id;

  @Enumerated(value = EnumType.STRING)
  private LangEnum lang;

  public LocalizedId(LangEnum lang) {
    this.lang = lang;
  }

}
