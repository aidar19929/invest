package kz.investgroup.invest.model.entity;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import kz.investgroup.invest.model.LangEnum;
import kz.investgroup.invest.model.PrivilegeEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Embeddable
@NoArgsConstructor
@EqualsAndHashCode
public class LocalizedPrivilegeId implements Serializable {

  private static final long serialVersionUID = 1089196571270403904L;

  @Enumerated(value = EnumType.STRING)
  private PrivilegeEnum id;

  @Enumerated(value = EnumType.STRING)
  private LangEnum lang;

  public LocalizedPrivilegeId(LangEnum lang) {
    this.lang = lang;
  }

}
