package kz.investgroup.invest.model.entity;

import static javax.persistence.CascadeType.DETACH;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import kz.investgroup.invest.model.LangEnum;
import kz.investgroup.invest.model.PrivilegeEnum;
import lombok.ToString;

@Entity
public class Privilege {

  @Id
  @Enumerated(value = EnumType.STRING)
  private PrivilegeEnum id;

  @ToString.Exclude
  @MapKey(name = "localizedId.lang")
  @OneToMany(mappedBy = "privilege", cascade = {DETACH, MERGE, PERSIST, REFRESH}, orphanRemoval = true, fetch = FetchType.EAGER)
  private Map<LangEnum, PrivilegeLocalization> localizations = new HashMap<>();

  @ToString.Exclude
  @ManyToMany(mappedBy = "privileges")
  private Collection<Role> roles;

  public String getName(LangEnum lang) {
    PrivilegeLocalization local = localizations.get(lang);
    return local == null ? null : local.getName();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Privilege privilege = (Privilege) o;
    return id == privilege.id;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

}
