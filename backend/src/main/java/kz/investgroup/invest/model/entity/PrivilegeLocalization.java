package kz.investgroup.invest.model.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity(name = "privilege_localization")
@EqualsAndHashCode(callSuper = true, exclude = "privilege")
public class PrivilegeLocalization extends AuditEntity implements Serializable {

  @EmbeddedId
  private LocalizedPrivilegeId localizedId;

  @ManyToOne
  @MapsId("id")
  @JoinColumn(name = "id")
  private Privilege privilege;

  @Column(name = "name")
  private String name;

}
