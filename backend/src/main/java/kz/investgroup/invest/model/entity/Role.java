package kz.investgroup.invest.model.entity;

import static javax.persistence.CascadeType.DETACH;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import kz.investgroup.invest.model.LangEnum;
import kz.investgroup.invest.model.RoleEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Entity(name = "role")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Role extends AuditEntity {

  @Id
  @Enumerated(value = EnumType.STRING)
  private RoleEnum id;

  @ToString.Exclude
  @MapKey(name = "localizedId.lang")
  @OneToMany(mappedBy = "role", cascade = {DETACH, MERGE, PERSIST, REFRESH}, orphanRemoval = true, fetch = FetchType.EAGER)
  private Map<LangEnum, RoleLocalization> localizations = new HashMap<>();

  @ToString.Exclude
  @ManyToMany(mappedBy = "roles")
  private List<Account> accounts = new ArrayList<>();

  @ToString.Exclude
  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(
      name = "role_privilege",
      joinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")},
      inverseJoinColumns = {@JoinColumn(name = "privilege_id", referencedColumnName = "id")}
  )
  private Set<Privilege> privileges = new HashSet<>();

  public String getName(LangEnum lang) {
    RoleLocalization local = localizations.get(lang);
    return local == null ? null : local.getName();
  }

}
