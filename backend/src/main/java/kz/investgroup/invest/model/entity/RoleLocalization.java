package kz.investgroup.invest.model.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity(name = "role_localization")
@EqualsAndHashCode(callSuper = true, exclude = "role")
public class RoleLocalization extends AuditEntity implements Serializable {

  @EmbeddedId
  private LocalizedRoleId localizedId;

  @ManyToOne
  @MapsId("id")
  @JoinColumn(name = "id")
  private Role role;

  @Column(name = "name")
  private String name;

}
