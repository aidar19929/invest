package kz.investgroup.invest.model.entity;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@Entity(name = "user_info")
@EqualsAndHashCode(callSuper = true)
public class UserInfo extends AuditEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "surname")
  private String surname;

  @Column(name = "name")
  private String name;

  @Column(name = "patronymic")
  private String patronymic;

  @ToString.Exclude
  @JoinColumn(name = "account_id")
  @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  private Account account;

  @ManyToOne
  @ToString.Exclude
  @JoinColumn(name = "company_id")
  private Company company;

  @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  @JoinTable(
      name = "user_info_work_position",
      joinColumns = {@JoinColumn(name = "user_info_id", referencedColumnName = "id")},
      inverseJoinColumns = {@JoinColumn(name = "work_position_id", referencedColumnName = "id")}
  )
  @ToString.Exclude
  private Set<WorkPosition> workPositions = new HashSet<>();

}
