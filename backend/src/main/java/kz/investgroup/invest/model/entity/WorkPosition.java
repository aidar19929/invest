package kz.investgroup.invest.model.entity;

import static javax.persistence.CascadeType.DETACH;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import kz.investgroup.invest.model.LangEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@Entity(name = "work_position")
@EqualsAndHashCode(callSuper = true)
public class WorkPosition extends AuditEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ToString.Exclude
  @MapKey(name = "localizedId.lang")
  @OneToMany(mappedBy = "workPosition", cascade = {DETACH, MERGE, PERSIST, REFRESH}, orphanRemoval = true)
  private Map<LangEnum, WorkPositionLocalization> localizations = new HashMap<>();

  @ToString.Exclude
  @ManyToMany(mappedBy = "workPositions")
  private Set<UserInfo> users = new HashSet<>();

  public String getName(LangEnum lang) {
    WorkPositionLocalization local = localizations.get(lang);
    return local == null ? null : local.getName();
  }

}
