package kz.investgroup.invest.repository;

import kz.investgroup.invest.model.entity.Account;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends CrudRepository<Account, Long> {

  Account findByUsername(String username);

}
