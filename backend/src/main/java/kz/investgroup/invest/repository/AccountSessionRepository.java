package kz.investgroup.invest.repository;

import javax.transaction.Transactional;
import kz.investgroup.invest.model.entity.AccountSession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountSessionRepository extends JpaRepository<AccountSession, Long> {

  @Transactional
  void deleteAllByUsername(String username);

  AccountSession findByRefreshToken(String username);

}
