package kz.investgroup.invest.repository;

import kz.investgroup.invest.model.RoleEnum;
import kz.investgroup.invest.model.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, RoleEnum> {
  
}
