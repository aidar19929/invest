package kz.investgroup.invest.repository;

import java.util.List;
import kz.investgroup.invest.model.entity.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserInfoRepository extends JpaRepository<UserInfo, Long> {

  List<UserInfo> findAll();

}
