package kz.investgroup.invest.security;

import lombok.Data;

@Data
public class JwtRequest {

  private String username;
  private String password;
  private String refreshToken;

}
