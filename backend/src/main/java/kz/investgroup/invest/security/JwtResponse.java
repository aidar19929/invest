package kz.investgroup.invest.security;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class JwtResponse {

  private String token;
  private String refreshToken;

}
