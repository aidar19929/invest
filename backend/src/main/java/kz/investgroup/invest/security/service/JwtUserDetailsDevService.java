package kz.investgroup.invest.security.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import kz.investgroup.invest.model.RoleEnum;
import kz.investgroup.invest.model.entity.Account;
import kz.investgroup.invest.model.entity.Role;
import kz.investgroup.invest.repository.AccountRepository;
import kz.investgroup.invest.repository.RoleRepository;
import org.apache.logging.log4j.util.Strings;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class JwtUserDetailsDevService extends JwtUserDetailsService {

  private RoleRepository roleRepository;
  private AccountRepository accountRepository;
  // dev-invest
  private static final String DEV_PASSWORD = "$2a$12$HkPSVg1V73TSgTwEpNXWBuqweQOGnUjjSsP1y1Klo4sH33ezkpsZy";

  public JwtUserDetailsDevService(RoleRepository roleRepository, AccountRepository repository) {
    super(repository);
    this.roleRepository = roleRepository;
    this.accountRepository = repository;
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    if (Strings.isBlank(username)) {
      throwNotFoundException(username);
    }

    if (username.startsWith("dev-") && accountRepository.findByUsername(username) == null) {
      //role1-role2-role3...
      String roleString = username.replaceAll("dev-", "");

      Set<SimpleGrantedAuthority> authorities = new HashSet<>();
      authorities.add(new SimpleGrantedAuthority(RoleEnum.ROLE_BASIC.name()));

      Account account = Account.builder()
          .username(username)
          .password(DEV_PASSWORD)
          .roles(new HashSet<>(getRoles(roleString)))
          .build();

      accountRepository.save(account);
    }

    return super.loadUserByUsername(username);
  }

  private Set<Role> getRoles(String roleString) {
    List<Role> roles = roleRepository.findAll();

    return getRoleEnums(roleString).stream().map(r -> roles.stream().filter(rr -> rr.getId().equals(r)).findFirst())
        .filter(Optional::isPresent)
        .map(Optional::get)
        .collect(Collectors.toSet());
  }

  private ArrayList<RoleEnum> getRoleEnums(String roleString) {
    ArrayList<RoleEnum> roles = new ArrayList<>();
    Arrays.asList(roleString.split("-")).forEach(role -> {
      RoleEnum roleEnum = RoleEnum.parseOrNull(role);
      if (roleEnum != null) {
        roles.add(roleEnum);
      }
    });
    return roles;
  }

}
