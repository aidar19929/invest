package kz.investgroup.invest.security.service;

import java.util.ArrayList;
import java.util.List;
import kz.investgroup.invest.model.entity.Account;
import kz.investgroup.invest.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.util.Strings;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@RequiredArgsConstructor
public class JwtUserDetailsService implements UserDetailsService {

  private final AccountRepository repository;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    if (Strings.isBlank(username)) {
      throwNotFoundException(username);
    }

    Account account = repository.findByUsername(username);
    if (account == null) {
      throwNotFoundException(username);
    }

    // TODO: 22.11.2021 check for old token

    List<SimpleGrantedAuthority> authorities = new ArrayList<>();
    if (account.getRoles() != null) {
      account.getRoles().stream()
          .map(role -> new SimpleGrantedAuthority(String.valueOf(role.getId())))
          .forEach(authorities::add);
    }

    return new User(account.getUsername(), account.getPassword(), authorities);
  }

  protected void throwNotFoundException(String username) {
    throw new UsernameNotFoundException("Account not found with username: " + username);
  }

}
