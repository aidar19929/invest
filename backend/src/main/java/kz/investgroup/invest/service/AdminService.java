package kz.investgroup.invest.service;

import kz.investgroup.invest.model.account.AccountCreateRequest;
import kz.investgroup.invest.model.account.AccountCreateResponse;

public interface AdminService {

  AccountCreateResponse createAccount(AccountCreateRequest request);

  void blockAccount(Long accountId);

  void unblockAccount(Long accountId);

}
