package kz.investgroup.invest.service;

import kz.investgroup.invest.security.JwtRequest;
import kz.investgroup.invest.security.JwtResponse;

public interface AuthService {

  JwtResponse getToken(JwtRequest request);

  JwtResponse refreshToken(String refreshToken);

}
