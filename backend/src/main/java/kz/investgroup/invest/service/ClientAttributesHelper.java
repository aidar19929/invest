package kz.investgroup.invest.service;

import kz.investgroup.invest.filter.RequestContextFilter.RequestContext;
import kz.investgroup.invest.model.LangEnum;

public interface ClientAttributesHelper {

  String getRequestId();

  LangEnum getLang();

  RequestContext getRequestContext();

}
