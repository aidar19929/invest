package kz.investgroup.invest.service.impl;

import kz.investgroup.invest.model.account.AccountCreateRequest;
import kz.investgroup.invest.model.account.AccountCreateResponse;
import kz.investgroup.invest.repository.AccountRepository;
import kz.investgroup.invest.repository.CompanyRepository;
import kz.investgroup.invest.service.AdminService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AdminServiceImpl implements AdminService {

  private final AccountRepository accountRepository;
  private final CompanyRepository companyRepository;


  @Override
  public AccountCreateResponse createAccount(AccountCreateRequest request) {
    return null;
  }

  @Override
  public void blockAccount(Long accountId) {
    throw new UnsupportedOperationException("not implemented");
  }

  @Override
  public void unblockAccount(Long accountId) {
    throw new UnsupportedOperationException("not implemented");
  }

}
