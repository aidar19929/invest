package kz.investgroup.invest.service.impl;

import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import kz.investgroup.invest.model.entity.Account;
import kz.investgroup.invest.model.entity.AccountSession;
import kz.investgroup.invest.repository.AccountRepository;
import kz.investgroup.invest.repository.AccountSessionRepository;
import kz.investgroup.invest.security.JwtRequest;
import kz.investgroup.invest.security.JwtResponse;
import kz.investgroup.invest.security.JwtTokenUtil;
import kz.investgroup.invest.security.UnauthorizedException;
import kz.investgroup.invest.service.AuthService;
import kz.investgroup.invest.service.DateHelper;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.util.Strings;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

  private final DateHelper dateHelper;
  private final JwtTokenUtil jwtTokenUtil;
  private final AuthenticationManager authenticationManager;
  private final AccountRepository accountRepository;
  private final AccountSessionRepository sessionRepository;

  @Override
  public JwtResponse getToken(JwtRequest request) {

    Authentication authenticate = authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword())
    );

    final String token = jwtTokenUtil.generateToken(request.getUsername(), authenticate.getAuthorities());

    sessionRepository.deleteAllByUsername(request.getUsername());

    long refreshTokenValidityMinutes = jwtTokenUtil.getJwtRefreshTokenValidityMinutes(authenticate.getAuthorities());
    AccountSession session = AccountSession.builder()
        .username(request.getUsername())
        .refreshToken(UUID.randomUUID().toString())
        .expiryDate(dateHelper.getNow().plus(refreshTokenValidityMinutes, ChronoUnit.MINUTES))
        .build();

    sessionRepository.save(session);

    return JwtResponse.builder()
        .token(token)
        .refreshToken(session.getRefreshToken())
        .build();
  }

  @Override
  public JwtResponse refreshToken(String refreshToken) {
    if (Strings.isBlank(refreshToken)) {
      throw new UnauthorizedException(new InsufficientAuthenticationException("Null or empty refresh token!"));
    }

    AccountSession oldSession = sessionRepository.findByRefreshToken(refreshToken);
    if (oldSession == null) {
      throw new UnauthorizedException(new InsufficientAuthenticationException("Session not found!"));
    }
    if (oldSession.getExpiryDate().isBefore(dateHelper.getNow())) {
      sessionRepository.delete(oldSession);
      throw new UnauthorizedException(new InsufficientAuthenticationException("Session expired!"));
    }
    String username = oldSession.getUsername();

    Account account = accountRepository.findByUsername(username);

    long refreshTokenValidityMinutes = jwtTokenUtil.getJwtRefreshTokenValidityMinutes(account.getRoles());
    AccountSession newSession = AccountSession.builder()
        .username(username)
        .refreshToken(UUID.randomUUID().toString())
        .expiryDate(dateHelper.getNow().plus(refreshTokenValidityMinutes, ChronoUnit.MINUTES))
        .build();

    sessionRepository.delete(oldSession);
    sessionRepository.save(newSession);

    List<SimpleGrantedAuthority> authorities = new ArrayList<>();
    if (account.getRoles() != null) {
      account.getRoles().stream()
          .map(role -> new SimpleGrantedAuthority(String.valueOf(role.getId())))
          .forEach(authorities::add);
    }

    return JwtResponse.builder()
        .token(jwtTokenUtil.generateToken(account.getUsername(), authorities))
        .refreshToken(newSession.getRefreshToken())
        .build();
  }

}
