package kz.investgroup.invest.service.impl;

import kz.investgroup.invest.filter.RequestContextFilter;
import kz.investgroup.invest.filter.RequestContextFilter.RequestContext;
import kz.investgroup.invest.model.LangEnum;
import kz.investgroup.invest.service.ClientAttributesHelper;
import org.springframework.stereotype.Service;

@Service
public class ClientAttributesHelperImpl implements ClientAttributesHelper {

  @Override
  public String getRequestId() {
    return getContext().getRequestId();
  }

  @Override
  public LangEnum getLang() {
    return getContext().getLang();
  }

  @Override
  public RequestContext getRequestContext() {
    return getContext();
  }

  private RequestContext getContext() {
    RequestContext context = RequestContextFilter.CONTEXT.get();
    return context;
  }

}
