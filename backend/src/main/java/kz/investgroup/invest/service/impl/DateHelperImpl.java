package kz.investgroup.invest.service.impl;

import java.time.LocalDateTime;
import kz.investgroup.invest.service.DateHelper;
import org.springframework.stereotype.Service;

@Service
public class DateHelperImpl implements DateHelper {

  @Override
  public LocalDateTime getNow() {
    return LocalDateTime.now();
  }

}
