package kz.investgroup.invest.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import kz.investgroup.invest.validation.validator.PatronymicValidator;

@Documented
@Constraint(validatedBy = PatronymicValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Patronymic {

  String message() default "{validation.patronymic}";

  int min() default 0;

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};

}
