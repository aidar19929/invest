set client_encoding='UTF8';

-- Роли
insert into role (id) values ('ROLE_SYSTEM');
insert into role_localization(id, lang, name) values('ROLE_SYSTEM', 'RU','Система');

insert into role (id) values ('ROLE_ADMIN');
insert into role_localization(id, lang, name) values('ROLE_ADMIN', 'RU','Админ');

insert into role (id) values ('ROLE_MODERATOR');
insert into role_localization(id, lang, name) values('ROLE_MODERATOR', 'RU','Модератор');

insert into role (id) values ('ROLE_INVESTOR');
insert into role_localization(id, lang, name) values('ROLE_INVESTOR', 'RU','Инвестор');

insert into role (id) values ('ROLE_BASIC');
insert into role_localization(id, lang, name) values('ROLE_BASIC', 'RU','Обычный пользователь');

insert into role (id) values ('ROLE_LEVEL_LOCAL');
insert into role_localization(id, lang, name) values('ROLE_LEVEL_LOCAL', 'RU','Лицо местного уровня');

insert into role (id) values ('ROLE_LEVEL_CENTRAL');
insert into role_localization(id, lang, name) values('ROLE_LEVEL_CENTRAL', 'RU','Лицо центрального уровня');

insert into role (id) values ('ROLE_LEVEL_AUTHORISED');
insert into role_localization(id, lang, name) values('ROLE_LEVEL_AUTHORISED', 'RU','Лицо уполномоченного органа');

insert into role (id) values ('ROLE_LEVEL_COMMISSION');
insert into role_localization(id, lang, name) values('ROLE_LEVEL_COMMISSION', 'RU','Лицо комиссии');


-- Права
insert into privilege(id) values('CREATE_ACCOUNT');
insert into privilege_localization(id, lang, name) values('CREATE_ACCOUNT', 'RU','Создание аккаунта');

insert into privilege(id) values('READ_ACCOUNT');
insert into privilege_localization(id, lang, name) values('READ_ACCOUNT', 'RU','Чтение данных аккаунта аккаунта');

insert into privilege(id) values('EDIT_ACCOUNT');
insert into privilege_localization(id, lang, name) values('EDIT_ACCOUNT', 'RU','Редактирование данных аккаунта');

insert into privilege(id) values('CREATE_PROJECT');
insert into privilege_localization(id, lang, name) values('CREATE_PROJECT', 'RU','Создание проекта');

insert into privilege(id) values('READ_PROJECT');
insert into privilege_localization(id, lang, name) values('READ_PROJECT', 'RU','Чтение проектов');

insert into privilege(id) values('EDIT_PROJECT');
insert into privilege_localization(id, lang, name) values('EDIT_PROJECT', 'RU','Редактирование проектов');

insert into privilege(id) values('APPROVE_LEVEL_LOCAL');
insert into privilege_localization(id, lang, name) values('APPROVE_LEVEL_LOCAL', 'RU', 'Согласование заявок на этапе местного уровня');

insert into privilege(id) values('APPROVE_LEVEL_CENTRAL');
insert into privilege_localization(id, lang, name) values('APPROVE_LEVEL_CENTRAL', 'RU', 'Согласование заявок на этапе центрального уровня');

insert into privilege(id) values('APPROVE_LEVEL_AUTHORISED');
insert into privilege_localization(id, lang, name) values('APPROVE_LEVEL_AUTHORISED', 'RU', 'Согласование заявок на этапе уполномоченного уровня');

insert into privilege(id) values('APPROVE_LEVEL_COMMISSION');
insert into privilege_localization(id, lang, name) values('APPROVE_LEVEL_COMMISSION', 'RU', 'Согласование заявок на этапе комиссии');


-- Связка Роль-Права
insert into role_privilege (role_id, privilege_id) values ('ROLE_SYSTEM', 'CREATE_ACCOUNT');
insert into role_privilege (role_id, privilege_id) values ('ROLE_SYSTEM', 'READ_ACCOUNT');
insert into role_privilege (role_id, privilege_id) values ('ROLE_SYSTEM', 'EDIT_ACCOUNT');
insert into role_privilege (role_id, privilege_id) values ('ROLE_SYSTEM', 'CREATE_PROJECT');
insert into role_privilege (role_id, privilege_id) values ('ROLE_SYSTEM', 'READ_PROJECT');
insert into role_privilege (role_id, privilege_id) values ('ROLE_SYSTEM', 'EDIT_PROJECT');
insert into role_privilege (role_id, privilege_id) values ('ROLE_SYSTEM', 'APPROVE_LEVEL_LOCAL');
insert into role_privilege (role_id, privilege_id) values ('ROLE_SYSTEM', 'APPROVE_LEVEL_CENTRAL');
insert into role_privilege (role_id, privilege_id) values ('ROLE_SYSTEM', 'APPROVE_LEVEL_AUTHORISED');
insert into role_privilege (role_id, privilege_id) values ('ROLE_SYSTEM', 'APPROVE_LEVEL_COMMISSION');

insert into role_privilege (role_id, privilege_id) values ('ROLE_ADMIN', 'READ_ACCOUNT');
insert into role_privilege (role_id, privilege_id) values ('ROLE_ADMIN', 'EDIT_ACCOUNT');
insert into role_privilege (role_id, privilege_id) values ('ROLE_ADMIN', 'CREATE_PROJECT');
insert into role_privilege (role_id, privilege_id) values ('ROLE_ADMIN', 'READ_PROJECT');
insert into role_privilege (role_id, privilege_id) values ('ROLE_ADMIN', 'EDIT_PROJECT');
insert into role_privilege (role_id, privilege_id) values ('ROLE_ADMIN', 'APPROVE_LEVEL_LOCAL');
insert into role_privilege (role_id, privilege_id) values ('ROLE_ADMIN', 'APPROVE_LEVEL_CENTRAL');
insert into role_privilege (role_id, privilege_id) values ('ROLE_ADMIN', 'APPROVE_LEVEL_AUTHORISED');
insert into role_privilege (role_id, privilege_id) values ('ROLE_ADMIN', 'APPROVE_LEVEL_COMMISSION');

insert into role_privilege (role_id, privilege_id) values ('ROLE_MODERATOR', 'READ_PROJECT');
insert into role_privilege (role_id, privilege_id) values ('ROLE_MODERATOR', 'EDIT_PROJECT');
insert into role_privilege (role_id, privilege_id) values ('ROLE_MODERATOR', 'APPROVE_LEVEL_LOCAL');
insert into role_privilege (role_id, privilege_id) values ('ROLE_MODERATOR', 'APPROVE_LEVEL_CENTRAL');
insert into role_privilege (role_id, privilege_id) values ('ROLE_MODERATOR', 'APPROVE_LEVEL_AUTHORISED');
insert into role_privilege (role_id, privilege_id) values ('ROLE_MODERATOR', 'APPROVE_LEVEL_COMMISSION');

insert into role_privilege (role_id, privilege_id) values ('ROLE_INVESTOR', 'READ_PROJECT');
insert into role_privilege (role_id, privilege_id) values ('ROLE_BASIC', 'READ_PROJECT');

insert into role_privilege (role_id, privilege_id) values ('ROLE_LEVEL_LOCAL', 'READ_PROJECT');
insert into role_privilege (role_id, privilege_id) values ('ROLE_LEVEL_LOCAL', 'APPROVE_LEVEL_LOCAL');


insert into role_privilege (role_id, privilege_id) values ('ROLE_LEVEL_CENTRAL', 'READ_PROJECT');
insert into role_privilege (role_id, privilege_id) values ('ROLE_LEVEL_CENTRAL', 'APPROVE_LEVEL_CENTRAL');


insert into role_privilege (role_id, privilege_id) values ('ROLE_LEVEL_AUTHORISED', 'READ_PROJECT');
insert into role_privilege (role_id, privilege_id) values ('ROLE_LEVEL_AUTHORISED', 'APPROVE_LEVEL_AUTHORISED');

insert into role_privilege (role_id, privilege_id) values ('ROLE_LEVEL_COMMISSION', 'READ_PROJECT');
insert into role_privilege (role_id, privilege_id) values ('ROLE_LEVEL_COMMISSION', 'APPROVE_LEVEL_COMMISSION');

