set client_encoding='UTF8';

insert into account(id, username, password)
values ((select nextval('account_id_seq')), 'system','$2a$12$tzLic5QzwWVJHDvBrfaeNe.3OAmm6nrSUGTL3tz1AF9Zzfp/uqSae');

insert into user_info(surname, name, account_id)
values ('SYSTEM', 'SYSTEM', (select currval('account_id_seq')));

insert into account_role values ((select currval('account_id_seq')), 'ROLE_SYSTEM');