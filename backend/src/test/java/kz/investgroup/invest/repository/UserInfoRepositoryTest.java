package kz.investgroup.invest.repository;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;
import kz.investgroup.invest.model.entity.UserInfo;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@Disabled
@SpringBootTest
class UserInfoRepositoryTest {

  @Autowired
  private UserInfoRepository repository;

  @Test
  void name() {
    List<UserInfo> users = repository.findAll();

    assertNotNull(users);
  }

}