import React from 'react'
import { Switch, Route } from 'react-router-dom'
import { routes } from './routing'
import { GoToTop } from './services/go-to-top'

const App = () => {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      username: 'dev-invest',
      password: 'dev-invest'
    })
  }

  fetch('http://localhost:8081/auth', requestOptions)
    .then(res => res.json())
    .then(
      (result) => {
        console.log(result.token)
      },
      () => {
        console.log('FAILED')
      }
    )

  return <>
    <GoToTop />
    <Switch>
      {routes.map(({ path, component, isExact }) => (
        <Route
          key={path}
          path={path}
          exact={isExact}
          component={component}
        />
      ))}
    </Switch>
  </>
}

export default App
