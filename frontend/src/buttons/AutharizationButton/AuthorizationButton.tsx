import React from 'react'
import { MENU_TYPES } from '../../constants'
import './AuthorizationButton.scss'

interface AuthorizationButtonProps {
  title: React.ReactElement | string
  visible?: boolean
  type?: string
}

export const AuthorizationButton = ({ title, visible, type }: AuthorizationButtonProps) => {
  const { mobile } = MENU_TYPES

  return (
    <button className={`authorization-button ${mobile === type ? 'mobile-button' : ''}`}
      style={{ display: visible ? 'none' : '' }}>
      {title}
    </button>
  )
}

// export default AuthorizationButton
