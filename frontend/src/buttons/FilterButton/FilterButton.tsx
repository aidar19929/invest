import React from 'react'
import filter from '../../assets/images/filter.svg'
import './filter-button.scss'

interface Props {
  title: React.ReactElement | string
  onClick: React.MouseEventHandler<HTMLButtonElement> | undefined
}

export const FilterButton = ({ title, onClick }: Props) => {
  return <button
      className="filter-button controller-button"
      onClick={onClick}
  >
    <img
        className="filter-button__img"
        src={filter}
        alt='filter img' />
    <span>{title}</span>
  </button>
}
