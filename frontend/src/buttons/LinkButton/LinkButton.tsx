import React from 'react'

interface LinkButtonProps {
  title: string
}

const LinkButton = ({ title }: LinkButtonProps) => (
  <div className="link-button">
    {title}
  </div>
)

export default LinkButton
