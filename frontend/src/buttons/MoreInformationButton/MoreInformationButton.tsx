import React from 'react'
import './moreInformationButton.scss'

interface Props {
    title: React.ReactElement | string
}

export const MoreInformationButton = ({ title }: Props) => (
    <button className="more-information-button">
        {title}
    </button>
)
