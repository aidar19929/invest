import React from 'react'
import './projectsToGridButton.scss'
import grid from '../../assets/images/grid.svg'

interface Props {
  title: React.ReactElement | string
  onClick: Function
}

export const ProjectsToGridButton = ({ title, onClick }: Props) => (
  <button
    className="to-grid-button controller-button"
    onClick={() => onClick()}>
    <img
      className="to-grid-button__grid-icon"
      src={grid}
      alt='grid icon' />
    <span>{title}</span>
  </button>
)
