import React from 'react'
import './projectsToListButton.scss'
import list from '../../assets/images/list.svg'

interface Props {
    title: React.ReactElement | string
    onClick: Function
}

export const ProjectsToListButton = ({ title, onClick }: Props) => (
    <button
        className="to-list-button controller-button"
        onClick={() => onClick()}>
        <img
            className="to-list-button__list-icon"
            src={list}
            alt='list icon' />
        <span>{title}</span>
    </button>
)
