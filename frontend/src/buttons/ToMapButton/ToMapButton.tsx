import React from 'react'
import './toMapButton.scss'
import mapPoint from '../../assets/images/map-point.svg'
import { PATH_MAP } from '../../routing'
import { history } from '../../index'

interface Props {
  title: React.ReactElement | string
  path?: string
}

export const ToMapButton = ({ title, path = PATH_MAP }: Props) => {
  const toMapOnClick = () => {
    history.push(path)
  }

  return <button
    className="to-map-button controller-button"
    onClick={toMapOnClick}>
    <img
      className="__map-point"
      src={mapPoint}
      alt='map point' />
    <span>{title}</span>
  </button>
}
