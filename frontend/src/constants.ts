export const DEVICES = {
  desktop: 'desktop',
  laptop: 'laptop',
  tablet: 'tablet',
  mobile: 'mobile'
}

export const MENU_TYPES = {
  mobile: 'mobile',
  footer: 'footer'
}

export const PROJECT_STAGES = {
  done: 'done',
  implemented: 'implemented',
  perspective: 'perspective'
}

export const PROJECT_NAMES = {
  kainarAcc: 'kainarAcc',
  kamLitKZ: 'KamLitKZ',
  centralAsiaCable: 'Hua Tun Central Asia cable',
  mabetexGroup: 'Mabetex Group',
  sapaInvestPlus: 'Sapa Invest Plus',
  bolashakElectric: 'Bolashak Electric'
}

export const PROJECTS_MANUFACTURED_PRODUCTS = {
  battery: 'battery',
  carAxles: 'car axles',
  cable: 'cable',
  ceramics: 'ceramics',
  electricalProducts: 'electrical products'
}

export const PROJECTS_REGIONS = {
  westKazakhstan: {
    name: 'West Kazakhstan',
    geometry: [49.882814, 50.791919]
  },
  kostanayRegion: {
    name: 'Kostanay region',
    geometry: [51.571616, 64.098132]
  },
  karagandaRegion: {
    name: 'Karaganda region',
    geometry: [48.283376, 70.923056]
  },
  akmolaRegion: {
    name: 'Akmola region',
    geometry: [52.400372, 70.213663]
  }
}

export const PROJECTS_CITYES = {
  uralsk: {
    name: 'Uralsk',
    geometry: [51.203995, 51.370505]
  },
  astana: {
    name: 'Astana',
    geometry: [51.128038, 71.430307]
  },
  almaty: {
    name: 'Almaty',
    geometry: [43.237100, 76.945526]
  },
  kastonay: {
    name: 'Kostanay',
    geometry: [53.214537, 63.631750]
  }
}
