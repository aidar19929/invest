import React from 'react'

interface BodyProps {
  children?: React.ReactNode
}

const Body = ({ children }: BodyProps) => (
  <div className="body">
    {children}
  </div>
)

export default Body
