import React from 'react'
import { AuthorizationButton } from '../../buttons/AutharizationButton'
import './Header.scss'

// // @ts-ignore
// import logo from '../../assets/images/logo.png'
import { useTranslation } from 'react-i18next'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../../redux/rootReducer'
import { changeLanguage } from '../../redux/actions'
import { NavsBar } from '../../views/Navs-bar'
import { Link } from 'react-router-dom'
import { ALL_LANGUAGES } from './constants'
import { DEVICES, MENU_TYPES } from '../../constants'
import { NAVS_ITEMS, PATH_HOME } from '../../routing'
import {
  MenuItem,
  Select
} from '@mui/material'

export const Header = () => {
  const dispatch = useDispatch()
  const appLanguage = useSelector<RootState, string>(state => state.app.language)
  const { t } = useTranslation()
  const device = useSelector<RootState, string>(state => state.app.device)
  const { desktop } = DEVICES

  return (
    <div className="main-header section">
      <div className="main-header__container container">
        {/* <div className="main-header__controller"> */}
        <div className="main-header__logo-wrap">
          {/* <img className='logo-img' src={logo} alt='logo' /> */}
          <Link
            className='main-header__link-to-home'
            to={PATH_HOME}>
            <span>LOGO</span>
          </Link>
        </div>

        {/* <ul className="main-header__languages-list">
          {Object.entries(ALL_LANGUAGES).map(([language, title]) => (
            <li key={language}
              className={'languages-list__language' + (language === appLanguage ? ' active' : '')}
              onClick={() => dispatch(changeLanguage(language))}
            >
              {title}
            </li>
          ))}
        </ul> */}

        {
          device === desktop
            ? <NavsBar
              items={NAVS_ITEMS}
            />
            : <></>
        }

        <Select
          variant='standard'
          value={appLanguage}
          onChange={(e) => dispatch(changeLanguage(e.target.value))}
        >
          {Object.entries(ALL_LANGUAGES).map(([language, title]) => (
            <MenuItem key={title} value={language}>{title}</MenuItem>
          ))}
        </Select>
        <AuthorizationButton
          visible={device !== desktop}
          title={
            t('header.authorization-button')
          }
        />
        <div className={'main-header__hamburger-menu-wrap'}
          style={{ display: device === desktop ? 'none' : '' }}>
          <div className="main-header__hamburger-menu">
            <input id="menu__toggle" type="checkbox" />
            <label className="menu__btn" htmlFor="menu__toggle">
              <span></span>
            </label>

            <div className="menu__box">
              <NavsBar
                items={NAVS_ITEMS}
                type={MENU_TYPES.mobile}
              />
              <AuthorizationButton
                type={MENU_TYPES.mobile}
                title={
                  t('header.authorization-button')
                }
              />
            </div>
          </div>
        </div>
        {/* </div> */}
      </div>
    </div>
  )
}
