import React from 'react'

interface WindowProps {
  children?: React.ReactNode
}

const Window = ({ children }: WindowProps) => (
  <div className="window">
    {children}
  </div>
)

export default Window
