import React from 'react'
import ReactDOM from 'react-dom'
import { Router } from 'react-router'
import App from './App'
import { Provider } from 'react-redux'
import store from './redux/store'
import { fontLoaded } from './redux/actions'
import { createBrowserHistory } from 'history'
import './services/i18n'
import './index.scss'

export const history = createBrowserHistory()
const WebFont = require('webfontloader')

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Router history={history}>
        <App />
      </Router>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
)

WebFont.load({
  google: {
    families: ['Montserrat', 'Open Sans', 'Rubik']
  },
  active: () => store.dispatch(fontLoaded())
})

// store.dispatch(fontLoaded())
