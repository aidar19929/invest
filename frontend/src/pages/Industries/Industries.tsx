import React, { useState } from 'react'
import { Footer } from '../../containers/Footer'
import { Header } from '../../containers/Header'
import { AllIndustries } from '../../views/All-industries'
import { Main } from '../../views/Main'
import { PageController } from '../../views/Page-controller'

export const Industries = () => {
  const [layautIsList, setLayautIsList] = useState(true)

  function changeLayaut () {
    setLayautIsList(!layautIsList)
  }

  return (
    <>
      <Header />
      <Main>
        <PageController
          onClickToList={changeLayaut}
          onClickToGrid={changeLayaut}
        />

        <AllIndustries
          layautIsList={layautIsList}
        />
      </Main>
      <Footer />
    </>
  )
}
