import React from 'react'
import { Footer } from '../../containers/Footer'
import { Header } from '../../containers/Header'
import { Main } from '../../views/Main'
import { Description } from '../../views/Description'
import { IndustriesSlider } from '../../views/Industries-slider'
import { TextAbout } from '../../views/TextAbout'
import { Partners } from '../../views/Partners'
import { InvestorsMap } from '../../views/Investors-map'
// import { Overview } from '../../views/Overview';
import { ProjectsSlider } from '../../views/Projects-slider'

export const Landing = () => {
  return (
    <>
      <Header />
      <Main>
        {/* <Overview /> */}
        <Description />
        <IndustriesSlider />
        <ProjectsSlider />
        <InvestorsMap />
        <TextAbout />
        <Partners />
      </Main>
      <Footer />
    </>
  )
}
