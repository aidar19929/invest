import React, { useState } from 'react'
import { Footer } from '../../containers/Footer'
import { Header } from '../../containers/Header'
import { InvestorsMap } from '../../views/Investors-map'
import { Main } from '../../views/Main'
import { PageController } from '../../views/Page-controller'
import { CARDS } from '../../views/Projects-slider/constsnts'
import './map.scss'

export const Map = () => {
  const [projects, setProjects] = useState(CARDS)

  return (
    <>
      <Header />
      <Main>
        <PageController
          toMapHidden
          setProjects={setProjects}
        />
        <InvestorsMap
          projects={projects}
          hideLinkToMap />
      </Main>
      <Footer />
    </>
  )
}
