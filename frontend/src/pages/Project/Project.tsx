import React, { useState } from 'react'
import { Footer } from '../../containers/Footer'
import { Header } from '../../containers/Header'
import { Main } from '../../views/Main'
import { PageController } from '../../views/Page-controller'
import { ProjectCard } from '../../views/Project-card'
import './project.scss'

export const Project = ({ project }: any) => {
  const [layautIsList, setlayautIsList] = useState(false)

  function changeLayaut () {
    setlayautIsList(!layautIsList)
  }

  return (
    <>
      <Header />
      <Main>
        <PageController
          onClickToList={changeLayaut}
          onClickToGrid={changeLayaut}
        />
        <div className='project-page section'>
          <div className='project-page__wrap container'>
            <ProjectCard
              layautIsList={layautIsList}
              project={project}
              allInformation
            />
          </div>
        </div>
      </Main>
      <Footer />
    </>
  )
}
