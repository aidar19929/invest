import React, { useState } from 'react'
import { Footer } from '../../containers/Footer'
import { Header } from '../../containers/Header'
import { AllProjects } from '../../views/All-projects'
import { Main } from '../../views/Main'
import { PageController } from '../../views/Page-controller'
import { CARDS } from '../../views/Projects-slider/constsnts'

export const Projects = () => {
  const [layautIsList, setLayautIsList] = useState(false)
  const [projects, setProjects] = useState(CARDS)

  function changeLayaut () {
    setLayautIsList(!layautIsList)
  }

  return (
    <>
      <Header />
      <Main>
        <PageController
          onClickToList={changeLayaut}
          onClickToGrid={changeLayaut}
          setProjects={setProjects}
        />

        <AllProjects
          layautIsList={layautIsList}
          projects={projects}
        />
      </Main>
      <Footer />
    </>
  )
}
