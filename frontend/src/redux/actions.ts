import { actionTypes } from './types'
import LanguageStorage from '../services/LanguageStorage'
import { AnyAction, Dispatch } from 'redux'
import i18next from 'i18next'
import { DEVICES } from '../constants'
import state from './store'

const { laptop, mobile, tablet, desktop } = DEVICES

export const fontLoaded = () => ({ type: actionTypes.APP_FONT_HAS_LOAD })

export function setLanguage (language: string): AnyAction {
  return { type: actionTypes.APP_SET_LANGUAGE, payload: language }
}

export function changeLanguage (language: string) {
  return async (dispatch: Dispatch) => {
    LanguageStorage.setLanguage(language)
    dispatch(setLanguage(language))
    await i18next.changeLanguage(language)
  }
}

let prevClientWidth = document.documentElement.clientWidth

function setDevice () {
  let device = ''
  if (prevClientWidth > 1024) {
    device = desktop
  }

  if (prevClientWidth <= 1024 && prevClientWidth > 768) {
    device = laptop
  }

  if (prevClientWidth <= 768 && prevClientWidth > 480) {
    device = tablet
  }

  if (prevClientWidth <= 480) {
    device = mobile
  }

  state.dispatch({ type: actionTypes.APP_SET_DEVICE, payload: device })
}

setDevice()

window.addEventListener('resize', () => {
  const resizeWidth = document.documentElement.clientWidth
  // if ((resizeWidth > prevClientWidth + 10) || (resizeWidth < prevClientWidth - 10)) {
  prevClientWidth = resizeWidth
  setDevice()
  // }
})
