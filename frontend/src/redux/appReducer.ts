import { actionTypes } from './types'
import { AnyAction } from 'redux'
import LanguageStorage from '../services/LanguageStorage'

export interface AppState {
  fontLoaded: boolean
  language: string
  device: string
}

const initialState: AppState = {
  fontLoaded: false,
  language: LanguageStorage.getLanguage(),
  device: ''
}

export const appReducer = (state = initialState, action: AnyAction) => {
  switch (action.type) {
    case actionTypes.APP_FONT_HAS_LOAD:
      return { ...state, fontLoaded: true }
    case actionTypes.APP_SET_LANGUAGE:
      return { ...state, language: action.payload }
    case actionTypes.APP_SET_DEVICE:
      return { ...state, device: action.payload }
    default:
      return state
  }
}
