export enum actionTypes {
  // eslint-disable-next-line no-unused-vars
  APP_FONT_HAS_LOAD,
  // eslint-disable-next-line no-unused-vars
  APP_SET_LANGUAGE,
  // eslint-disable-next-line no-unused-vars
  APP_SET_DEVICE,
}
