import React, { Component } from 'react'
import { Industries } from './pages/Industries'
import { Landing } from './pages/Landing'
import { Map } from './pages/Map'
import { Project } from './pages/Project'
import { Projects } from './pages/Projects'
import { CARDS } from './views/Projects-slider/constsnts'

export const PATH_HOME = '/'
export const PATH_PROJECTS = '/projects'
export const PATH_INDUSTRIES = '/industries'
export const PATH_MAP = '/map'
export const routes = [
  {
    path: PATH_HOME,
    title: 'Home',
    isExact: true,
    component: Landing
  },
  {
    path: PATH_PROJECTS,
    title: 'Projects',
    isExact: true,
    component: Projects
  },
  {
    path: PATH_INDUSTRIES,
    title: 'Industries',
    isExact: true,
    component: Industries
  },
  {
    path: PATH_MAP,
    title: 'Map',
    isExact: true,
    component: Map
  },

  ...CARDS.map((project) => {
    return {
      path: `/${project.id}`,
      title: `${project.projectName}`,
      isExact: true,
      component: class Example extends Component {
        render () {
          return (
            <Project
              project={project} />
          )
        }
      }
    }
  })

  // {
  //   path: PATH_REGISTRATION,
  //   title: 'Registration',
  //   isExact: false,
  //   component: IdentificationContainer,
  // },
]

export const NAVS_ITEMS = [
  // 'news',
  // 'Investor',
  // 'Regions of Kazakhstan',
  // 'Analytics',
  // 'Partners',
  // 'about-project'
  {
    name: 'Landing',
    path: PATH_HOME
  },
  {
    name: 'Map',
    path: PATH_MAP
  },
  {
    name: 'Projects',
    path: PATH_PROJECTS
  },
  {
    name: 'Industries',
    path: PATH_INDUSTRIES
  }
]
