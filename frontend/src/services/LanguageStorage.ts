import config from './Config'

export default new (class {
  getLanguage = () => {
    let language = localStorage.getItem('currentLanguage')

    if (language === '' || language === 'undefined' || typeof language === 'undefined') {
      language = null
    }

    return language !== null ? language : config.getDefaultLanguage()
  }

  setLanguage = (language: any) => localStorage.setItem('currentLanguage', language)
})()
