import i18next from 'i18next'
import { initReactI18next } from 'react-i18next'
import LanguageDetector from 'i18next-browser-languagedetector'
import languageStorage from './LanguageStorage'
import backend from 'i18next-xhr-backend'
import translationEn from '../assets/translations/en/translation.json'
import translationKz from '../assets/translations/kz/translation.json'
import translationRu from '../assets/translations/ru/translation.json'

i18next
  .use(LanguageDetector)
  .use(backend)
  .use(initReactI18next)
  .init({
    fallbackLng: 'en',
    lng: languageStorage.getLanguage(),
    debug: true,
    detection: {
      order: ['queryString', 'cookie'],
      caches: ['cookie']
    },
    interpolation: {
      escapeValue: false
    },
    resources: {
      en: {
        translation: translationEn
      },
      kz: {
        translation: translationKz
      },
      ru: {
        translation: translationRu
      }
    }
  })

export default i18next
