import React from 'react'

interface UnderlinedTextProps {
    text: string
    lineColor: string
    className?: string
    startLine?: number // 0 <-> 100, by default: 0
}

const UnderlinedText = ({text, lineColor, className, startLine = 100}: UnderlinedTextProps) => {
    return (
        <div className={['underlined-text', className].join(' ')}>
            <div className="underlined-text-container">
                {text}
                <div className="line" style={{width: (100 - startLine) + '%'}}/>
            </div>
        </div>
    )
}

export default UnderlinedText