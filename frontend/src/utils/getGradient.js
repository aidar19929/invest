export const getGradient = (ctx, chartArea, colors) => {
    const gradient = ctx.createLinearGradient(0, chartArea.top, 0, chartArea.bottom);
    gradient.addColorStop(0, colors[0]);
    gradient.addColorStop(1, colors[1]);

    return gradient;
}