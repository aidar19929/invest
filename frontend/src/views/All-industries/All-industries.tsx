import React from 'react'
import { IndustryCard } from '../Industry-card'
// import { useTranslation } from 'react-i18next';
import { CARDS } from '../Industries-slider/constsnts'

import './all-industries.scss'

interface Props {
  layautIsList: boolean
}

export const AllIndustries = ({ layautIsList }: Props) => {
  // const { t } = useTranslation()

  return (
    <section className='all-industries section'>
      <div className='all-industries__wrap container'>
        <ul className={`all-industries__industries ${layautIsList ? 'list-layaut' : ''}`}>
          {CARDS.map((card, i) =>
            <li key={i}>
              <IndustryCard
                key={i}
                card={card}
                layautIsList={layautIsList}
                goodsIsVisible
              />
            </li>)
          }
        </ul>
      </div>
    </section>
  )
}
