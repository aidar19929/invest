import React from 'react'
import { ProjectCard } from '../Project-card'
import { CARDS, CardsInterface } from '../Projects-slider/constsnts'

import './all-projects.scss'

interface Props {
  layautIsList: boolean
  projects?: CardsInterface
}

export const AllProjects = ({ layautIsList, projects = CARDS }: Props) => {
  return (
    <section className='all-rojects section'>
      <div className='all-rojects__wrap container'>
        <ul className={`all-rojects__rojects ${layautIsList ? 'list-layaut' : ''}`}>
          {projects.map((project, i) =>
            <li className='all-rojects__roject' key={i}>
              <ProjectCard
                key={i}
                layautIsList={layautIsList}
                project={project}
              />
            </li>)
          }
        </ul>
      </div>
    </section>
  )
}
