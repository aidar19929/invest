import React from 'react'

import './card-item.scss'

interface Prop {
  color: string
  percent: string
  text: string
}

export const CardItem = ({ color, percent, text }: Prop) => {
  return (

    <div className='card-item'>
      <div className='card-item__point' style={{ background: `${color}` }}></div>
      <span className='card-item__movement'>{percent}</span>
      <p className='card-item__text'>{text}</p>
    </div>
  )
}
