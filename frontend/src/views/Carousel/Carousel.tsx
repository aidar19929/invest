import React, { useEffect } from 'react'
import Glider from 'glider-js'
// documentation https://nickpiscitelli.github.io/Glider.js/
import './carousel.scss'

export const Carousel = () => {
  useEffect(() => {
    const glide = document.getElementById('glider') || document.createElement('div')

    // eslint-disable-next-line no-new
    new Glider(glide, {
      slidesToShow: 'auto',
      itemWidth: 320,
      exactWidth: true,
      resizeLock: false,
      draggable: true
    })
  })

  return (
    <div className="glider-contain">
      <div id="glider" className="glider">
        <div><div className='glider__card'></div></div>
        <div><div className='glider__card'></div></div>
        <div><div className='glider__card'></div></div>
        <div><div className='glider__card'></div></div>
        <div><div className='glider__card'></div></div>
        <div><div className='glider__card'></div></div>
        <div><div className='glider__card'></div></div>
        <div><div className='glider__card'></div></div>
        <div><div className='glider__card'></div></div>
        <div><div className='glider__card'></div></div>
        <div><div className='glider__card'></div></div>
      </div>
    </div>
  )
}
