import React from 'react'
import { useTranslation } from 'react-i18next'
import { ACTIVITIES } from './constsnts'

import './description.scss'

export const Description = () => {
  const { t } = useTranslation()

  return (
    <section className='description section'>
      <div className='description__wrap container'>
        <h2 className='description__title'>
          {t('description.description__title')}
        </h2>

        <p className='description__paragraph'>
          {t('description.description__paragraph')}
        </p>

        <div className='description__activities'>
          {ACTIVITIES.map((activity, i) => (
            <figure className='description__activity' key={i}>
              <div className='activity__img-wrap'>
                <img
                  className={'activity__img'}
                  src={activity.img}
                  alt='activity img'
                />
              </div>

              <figcaption
                className='activity__figcaption'>
                {t(`description.activities.${activity.title}`)}
              </figcaption>

              {/* <SkeletonItem width={20} height={37}>
                                <p className='activity__text'>
                                    {t(`description.activities.${activity.text}`)}
                                </p>
                            </SkeletonItem> */}
            </figure>
          ))}
        </div>
      </div>
    </section>
  )
}
