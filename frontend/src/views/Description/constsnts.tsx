import activity1 from '../../assets/images/activity-img-1.png'
import activity2 from '../../assets/images/activity-img-2.png'
import activity3 from '../../assets/images/activity-img-3.png'

export const ACTIVITIES = [
  {
    img: activity1,
    title: '0.title',
    text: '0.text'
  },
  {
    img: activity2,
    title: '1.title',
    text: '1.text'
  },
  {
    img: activity3,
    title: '2.title',
    text: '2.text'
  },
  {
    img: activity3,
    title: '2.title',
    text: '2.text'
  }
]
