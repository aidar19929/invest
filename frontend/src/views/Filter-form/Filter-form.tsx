import React, { useEffect, useState } from 'react'
import {
  Box,
  Button,
  // ButtonGroup,
  FormControl,
  InputLabel,
  MenuItem,
  Modal,
  Select,
  Slider,
  SliderThumb,
  styled
} from '@mui/material'
import {
  PROJECTS_CITYES,
  PROJECTS_MANUFACTURED_PRODUCTS,
  PROJECTS_REGIONS,
  PROJECT_STAGES
} from '../../constants'
import { CARDS, CardsInterface } from '../Projects-slider/constsnts'

import './filter-form.scss'
import { useTranslation } from 'react-i18next'

interface Prop {
  open: boolean
  returnFilteredProjects: any
  closeModal: any
}

const CostSlider = styled(Slider)(({ theme }) => ({
  color: '#3a8589',
  height: 3,
  padding: '13px 0',
  '& .MuiSlider-thumb': {
    height: 27,
    width: 27,
    backgroundColor: '#fff',
    border: '1px solid currentColor',
    '&:hover': {
      boxShadow: '0 0 0 8px rgba(58, 133, 137, 0.16)'
    },
    '& .airbnb-bar': {
      height: 9,
      width: 1,
      backgroundColor: 'currentColor',
      marginLeft: 1,
      marginRight: 1
    }
  },
  '& .MuiSlider-track': {
    height: 3
  },
  '& .MuiSlider-rail': {
    color: theme.palette.mode === 'dark' ? '#bfbfbf' : '#d8d8d8',
    opacity: theme.palette.mode === 'dark' ? undefined : 1,
    height: 3
  }
}))

interface CostSliderThumbComponentProps extends React.HTMLAttributes<unknown> { }

function CostSliderThumbComponent (props: CostSliderThumbComponentProps) {
  const { children, ...other } = props
  return (
    <SliderThumb {...other}>
      {children}
      <span className="airbnb-bar" />
      <span className="airbnb-bar" />
      <span className="airbnb-bar" />
    </SliderThumb>
  )
}

const minValueCost = CARDS.reduce((acc, next) => {
  if (next.projectCost < acc) {
    acc = next.projectCost
  }
  return acc
}, CARDS[0].projectCost)

const maxValueCost = CARDS.reduce((acc, next) => {
  if (next.projectCost > acc) {
    acc = next.projectCost
  }
  return acc
}, CARDS[0].projectCost)

const getCommissioningYears = () => {
  const maxCommissioningYear = CARDS.reduce((acc, next) => {
    if (next.сommissioningYear > acc) {
      acc = next.сommissioningYear
    }
    return acc
  }, CARDS[0].сommissioningYear)

  const minCommissioningYear = CARDS.reduce((acc, next) => {
    if (next.сommissioningYear < acc) {
      acc = next.сommissioningYear
    }
    return acc
  }, CARDS[0].сommissioningYear)

  const commissioningYears = [minCommissioningYear]

  while (commissioningYears[commissioningYears.length - 1] < maxCommissioningYear) {
    commissioningYears.push(commissioningYears[commissioningYears.length - 1] + 1)
  }

  return commissioningYears
}

const commissioningYears = getCommissioningYears()

const yearsMarks = [
  {
    value: minValueCost,
    label: minValueCost.toString()
  },
  {
    value: maxValueCost,
    label: maxValueCost.toString()
  }
]

const compareStages = (stageFromFilter: unknown | string, stageFromProject: string) =>
  !stageFromFilter || stageFromFilter === stageFromProject

const compareProducts = (productFromFilter: string | unknown, productsFromProject: string[]) =>
  !productFromFilter || !(productsFromProject.every((product: any) => product !== productFromFilter))

const compareRegions = (regionFromFilter: unknown | string, regionFromProject: string) =>
  !regionFromFilter || regionFromFilter === regionFromProject

const compareCities = (cityFromFilter: unknown | string, cityFromProject: string | undefined) =>
  !cityFromFilter || !cityFromProject || cityFromFilter === cityFromProject

const compareCosts = (costsFromFilter: number[], costFromProject: number) =>
  costsFromFilter[0] <= costFromProject && costsFromFilter[1] >= costFromProject

const compareYears = (yearFrom: number | string, yearTo: number | string, yearFromProject: number) =>
  yearFromProject >= yearFrom && yearFromProject <= yearTo

export function FilterForm ({ open, returnFilteredProjects, closeModal }: Prop) {
  const [stage, setStage] = useState<unknown | string>('')
  const [product, setProduct] = useState<unknown | string>('')
  const [region, setRegion] = useState<unknown | string>('')
  const [city, setCity] = useState<unknown | string>('')
  const [costs, setCosts] = useState<number[]>([minValueCost, maxValueCost])
  const [yearFrom, setYearFrom] = useState<number | string>(commissioningYears[0])
  const [yearTo, setYearTo] = useState<number | string>(commissioningYears[commissioningYears.length - 1])
  const [filteredProjects, setfFilteredProjects] = useState<CardsInterface>(CARDS)

  const { t } = useTranslation()

  useEffect(() => {
    const filteredProjects = CARDS.reduce((acc, next) => {
      if (
        compareStages(stage, next.stage) &&
        compareProducts(product, next.manufacturedProducts) &&
        compareRegions(region, next.region.name) &&
        compareCities(city, next.city?.name) &&
        compareCosts(costs, next.projectCost) &&
        compareYears(yearFrom, yearTo, next.сommissioningYear)
      ) {
        // @ts-ignore
        acc.push(next)
      }
      return acc
    }, [])
    setfFilteredProjects(filteredProjects)
    // console.log(filteredProjects)
  }, [
    stage,
    product,
    region,
    city,
    costs,
    yearFrom,
    yearTo
  ])

  function resetForm () {
    setStage('')
    setProduct('')
    setRegion('')
    setCity('')
    setCosts([minValueCost, maxValueCost])
    setYearFrom(commissioningYears[0])
    setYearTo(commissioningYears[commissioningYears.length - 1])
  }
  return (
    <Modal
      open={open}
      onBackdropClick={() => closeModal()}>
      <Box className='filter-form' >
        <div className='filter-form__container--first'>
          <FormControl fullWidth>
            <InputLabel className='filter-form__input-label' id="stage">
              {t('project-filter.stage')}
            </InputLabel>
            <Select
              variant="outlined"
              labelId="stage"
              value={stage}
              label={t('project-filter.stage')}
              onChange={(e) => setStage(e.target.value)}
            >
              <MenuItem value=''><em>None</em></MenuItem>
              {
                Object.values(PROJECT_STAGES).map((stage, i) =>
                  <MenuItem key={i} value={stage}>{t(`project-stages.${stage}`)}</MenuItem>)
              }
            </Select>
          </FormControl>

          <FormControl fullWidth>
            <InputLabel className='filter-form__input-label' id="product">
              {t('project-filter.product')}
            </InputLabel>
            <Select
              labelId="product"
              value={product}
              label={t('project-filter.product')}
              onChange={(e) => setProduct(e.target.value)}
            >
              <MenuItem value=''><em>None</em></MenuItem>
              {
                Object.values(PROJECTS_MANUFACTURED_PRODUCTS).map((product, i) =>
                  <MenuItem key={i} value={product}>{t(`projects_manufactured_products.${product}`)}</MenuItem>)
              }
            </Select>
          </FormControl>

          <FormControl fullWidth>
            <InputLabel className='filter-form__input-label' id="region">
              {t('project-filter.region')}
            </InputLabel>
            <Select
              labelId="region"
              value={region}
              label={t('project-filter.region')}
              onChange={(e) => setRegion(e.target.value)}
            >
              <MenuItem value=''><em>None</em></MenuItem>
              {
                Object.values(PROJECTS_REGIONS).map((region, i) =>
                  <MenuItem key={i} value={region.name}>{t(`projects_regions.${region.name}`)}</MenuItem>)
              }
            </Select>
          </FormControl>

          <FormControl fullWidth>
            <InputLabel className='filter-form__input-label' id="city">
              {t('project-filter.city')}
            </InputLabel>
            <Select
              labelId="city"
              value={city}
              label={t('project-filter.city')}
              onChange={(e) => setCity(e.target.value)}
            >
              <MenuItem value=''><em>None</em></MenuItem>
              {
                Object.values(PROJECTS_CITYES).map((city, i) =>
                  <MenuItem key={i} value={city.name}>{t(`projects_cityes.${city.name}`)}</MenuItem>)
              }
            </Select>
          </FormControl>
        </div>

        <div className='filter-form__container--second'>
          <div className='filter-form__select-cost'>
            <h4>{t('project-filter.cost')}</h4>
            <CostSlider
              components={{ Thumb: CostSliderThumbComponent }}
              defaultValue={costs}
              marks={yearsMarks}
              valueLabelDisplay='auto'
              // step={5}
              value={costs}
              // @ts-ignore
              onChange={(e) => setCosts(e.target.value)}
              min={minValueCost}
              max={maxValueCost}
            />
          </div>
          <div className='filter-form__select-years'>
            <h4 className='select-years__title'>
              {t('project-filter.select-years.title')}
            </h4>
            <div className='select-years__selects'>
              <div className='select-years__yer-from'>
                <InputLabel className='filter-form__input-label' id="from">
                  {t('project-filter.select-years.from')}
                </InputLabel>
                <Select
                  labelId="from"
                  value={yearFrom}
                  onChange={(e) => setYearFrom(e.target.value)}>
                  {commissioningYears.map((year, i) =>
                    <MenuItem key={i} value={year}>{year}</MenuItem>
                  )}
                </Select>
              </div>
              <div className='select-years__year-to'>
                <InputLabel className='filter-form__input-label' id="to">
                  {t('project-filter.select-years.to')}
                </InputLabel>
                <Select
                  labelId="to"
                  value={yearTo}
                  onChange={(e) => setYearTo(e.target.value)}>
                  {commissioningYears.map((year, i) =>
                    <MenuItem key={i} value={year}>{year}</MenuItem>
                  )}
                </Select>
              </div>
            </div>
          </div>
        </div>
        <p className='filter-form__additional-information'>
          <span className='additional-information__number-of-projects'>
            <span className='number-of-projects__text'>
              {t('project-filter.number-of-projects')}
            </span>
            <span className='number-of-projects__value'>
              {filteredProjects.length}
            </span>
          </span>
          <span className='additional-information__note'>
            {t('project-filter.additional-information__note')}
          </span>
        </p>
        <div className='filter-form__form-control'>
          <Button
            className='form-control__view-btn'
            onClick={() => returnFilteredProjects(filteredProjects)}>
            {t('project-filter.view-btn')}
          </Button>
          <Button
            className='form-control__reset-btn'
            onClick={() => resetForm()}>
            {t('project-filter.reset-btn')}
          </Button>
        </div>
      </Box>
    </Modal>
  )
}
