import React, { useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { DEVICES } from '../../constants'
import { useSelector } from 'react-redux'
import { RootState } from '../../redux/rootReducer'
import { Link } from 'react-router-dom'
import Glider from 'glider-js'
import { CARDS } from './constsnts'
import { PATH_INDUSTRIES } from '../../routing'
import { IndustryCard } from '../Industry-card'
// documentation https://nickpiscitelli.github.io/Glider.js/

import './industries-slider.scss'

export const IndustriesSlider = () => {
  const { mobile, tablet } = DEVICES
  const { t } = useTranslation()
  const device = useSelector<RootState, string>(state => state.app.device)

  useEffect(() => {
    const glide = document.getElementById('industries-slider__cards') || document.createElement('div')

    const glider = new Glider(glide, {
      slidesToShow: 'auto',
      itemWidth: 360,
      dots: '.dots',
      draggable: true,
      scrollLock: true
    })

    if (device === mobile || device === tablet) {
      glider.setOption({
        slidesToShow: 1
      })
    }

    if (device !== mobile && device !== tablet) {
      glider.setOption({
        slidesToShow: 'auto'
      })
    }

    glider.refresh(true)
  }, [device])

  return (
    <section className='industries-slider'>
      <div className='industries-slider__wrap container'>
        <h2 className='industries-slider__title'>{t('industries-slider.title')}</h2>
        <div className='industries-slider__cards' id={'industries-slider__cards'}>
          {
            CARDS.map((card, i) => (
              <IndustryCard
                key={i}
                card={card}
              />
            ))
          }
        </div>
        <div role="dots" className="dots"></div>

        <Link
          className='industries-slider__link-to-industries react-link'
          to={PATH_INDUSTRIES}>
          {t('industries-slider.link-to-industries')}
        </Link>
      </div>
    </section>
  )
}
