export const CARDS = [
  {
    id: '0',
    themeIswhite: true,
    marketValue: 20.7,
    marketCategoriesValues: {
      making: 4417,
      import: 17640
    },
    products: [
      {
        name: '0',
        value: 1242.1
      },
      {
        name: '1',
        value: 828.8
      },
      {
        name: '2',
        value: 709.2
      },
      {
        name: '3',
        value: 669.8
      },
      {
        name: '4',
        value: 628.8
      },
      {
        name: '5',
        value: 505.4
      },
      {
        name: '6',
        value: 482.0
      },
      {
        name: '7',
        value: 342.4
      },
      {
        name: '8',
        value: 334.2
      }
    ]
  },
  {
    id: '1',
    themeIswhite: true,
    marketValue: 3723,
    marketCategoriesValues: {
      making: 1549.9,
      import: 2366.7
    },
    products: [
      {
        name: '0',
        value: 482.0
      },
      {
        name: '1',
        value: 334.2
      },
      {
        name: '2',
        value: 147.2
      },
      {
        name: '3',
        value: 140.0
      },
      {
        name: '4',
        value: 139.2
      },
      {
        name: '5',
        value: 114.7
      },
      {
        name: '6',
        value: 112.1
      },
      {
        name: '7',
        value: 100.9
      },
      {
        name: '8',
        value: 62.4
      },
      {
        name: '9',
        value: 60.2
      }
    ]
  },
  {
    id: '2',
    themeIswhite: true,
    marketValue: 3545,
    marketCategoriesValues: {
      making: 164.8,
      import: 3450
    },
    products: [
      {
        name: '0',
        value: 1.421
      },
      {
        name: '1',
        value: 709.2
      },
      {
        name: '2',
        value: 301.6
      },
      {
        name: '3',
        value: 209.2
      },
      {
        name: '4',
        value: 148.5
      },
      {
        name: '5',
        value: 102.5
      },
      {
        name: '6',
        value: 37.9
      },
      {
        name: '7',
        value: 35.9
      },
      {
        name: '8',
        value: 32.6
      },
      {
        name: '9',
        value: 32.2
      }
    ]
  },
  {
    id: '3',
    themeIswhite: true,
    marketValue: 633.4,
    marketCategoriesValues: {
      making: 236.7,
      import: 405.9
    },
    products: [
      {
        name: '0',
        value: 82.0
      },
      {
        name: '1',
        value: 31.6
      },
      {
        name: '2',
        value: 27.1
      },
      {
        name: '3',
        value: 24.0
      },
      {
        name: '4',
        value: 23.7
      },
      {
        name: '5',
        value: 23.4
      },
      {
        name: '6',
        value: 21.3
      },
      {
        name: '7',
        value: 17.7
      },
      {
        name: '8',
        value: 17.3
      },
      {
        name: '9',
        value: 16.1
      }
    ]
  },
  {
    id: '4',
    themeIswhite: true,
    marketValue: 912.7,
    marketCategoriesValues: {
      making: 56.2,
      import: 903.7
    },
    products: [
      {
        name: '0',
        value: 117.9
      },
      {
        name: '1',
        value: 110.3
      },
      {
        name: '2',
        value: 106.4
      },
      {
        name: '3',
        value: 94.4
      },
      {
        name: '4',
        value: 83.9
      },
      {
        name: '5',
        value: 69.7
      },
      {
        name: '6',
        value: 46.0
      },
      {
        name: '7',
        value: 43.0
      },
      {
        name: '8',
        value: 38.6
      },
      {
        name: '9',
        value: 24.1
      }
    ]
  }
]

// export const CARDS = [
//     {
//         title: '0.title',
//         items: [
//             {
//                 color: 'red',
//                 percent: '4%',
//                 text: '0.items.0',
//             },
//             {
//                 color: 'green',
//                 percent: '55%',
//                 text: '0.items.1',
//             }
//         ],

//         themeIswhite: true
//     },
//     {
//         title: '1.title',
//         items: [
//             {
//                 color: 'red',
//                 percent: '4%',
//                 text: '1.items.0',
//             },
//             {
//                 color: 'green',
//                 percent: '55%',
//                 text: '1.items.1',
//             }
//         ],

//         themeIswhite: true
//     },
//     {
//         title: '2.title',
//         items: [
//             {
//                 color: 'red',
//                 percent: '4%',
//                 text: '2.items.0',
//             },
//             {
//                 color: 'green',
//                 percent: '55%',
//                 text: '2.items.1',
//             }
//         ],

//         themeIswhite: true
//     },
//     {
//         title: '3.title',
//         items: [
//             {
//                 color: 'red',
//                 percent: '4%',
//                 text: '3.items.0',
//             },
//             {
//                 color: 'green',
//                 percent: '55%',
//                 text: '3.items.1',
//             }
//         ],

//         themeIswhite: true
//     },
//     {
//         title: '4.title',
//         items: [
//             {
//                 color: 'red',
//                 percent: '4%',
//                 text: '4.items.0',
//             },
//             {
//                 color: 'green',
//                 percent: '55%',
//                 text: '4.items.1',
//             }
//         ],

//         themeIswhite: true
//     },
//     {
//         title: '5.title',
//         items: [
//             {
//                 color: 'red',
//                 percent: '4%',
//                 text: '5.items.0',
//             },
//             {
//                 color: 'green',
//                 percent: '55%',
//                 text: '5.items.1',
//             }
//         ],

//         themeIswhite: true
//     },
// ]
