import React from 'react'
import { useTranslation } from 'react-i18next'
import cardBackgroundImg from '../../assets/images/card-background.png'

import './industry-card.scss'

interface Prop {
  layautIsList?: boolean
  goodsIsVisible?: boolean
  card: {
    id: string,
    themeIswhite: boolean,
    marketValue: number,
    marketCategoriesValues: {
      making: number,
      import: number
    },
    products: {
      name: string,
      value: number,
    }[],
  }
}

export const IndustryCard = ({ card, layautIsList, goodsIsVisible }: Prop) => {
  const { t } = useTranslation()
  const {
    themeIswhite,
    marketValue,
    marketCategoriesValues,
    products,
    id
  } = card

  return (
    <div className='industry-card'>
      <div className={`industry-card__wrap ${themeIswhite ? 'white-style' : ''} ${layautIsList ? 'list-layaut' : ''}`}>
        <img
          className='card__card-background'
          src={cardBackgroundImg}
          alt='card background'
        />
        <h4 className='cart__tite'>
          {t(`industry-card.cards.${id}.title`)}
        </h4>

        <div className='card__domestic-market'>
          <div className='domestic-market__coommon-market'>
            <h3 className='coommon-market__title'>
              {t('industry-card.domestic-market__title')}
            </h3>
            <p className='coommon-market__value'>
              {`$${marketValue}`}
              <span className='coommon-value__measure'>
                {t(`industry-card.cards.${id}.market-value-measure`)}
              </span>
            </p>
          </div>

          <div className='domestic-market__categories'>
            <div className='domestic-market__making'>
              <p className='categories__categories-name'>
                {t('industry-card.domestic-market__making-name')}
              </p>
              <p
                className='categories__categories-value'>
                {`$${marketCategoriesValues.making}`}
              </p>
              <p className='categories__measure-categories'>
                {t('industry-card.categories__measure-categories')}
              </p>
            </div>

            <div className='domestic-market__import'>
              <p className='categories__categories-name'>
                {t('industry-card.domestic-market__import-name')}
              </p>
              <p
                className='categories__categories-value'>
                {`$${marketCategoriesValues.import}`}
              </p>
              <p className='categories__measure-categories'>
                {t('industry-card.categories__measure-categories')}
              </p>
            </div>
          </div>
        </div>

        <div className={`card__imported-goods ${themeIswhite ? 'imported-goods__white-style' : ''}`}
          style={{ display: `${goodsIsVisible ? 'flex' : 'none'}` }}>
          <h3 className='imported-goods__title'>
            {t('industry-card.imported-goods__title')}
          </h3>
          <div className='imported-goods__goods'>
            {products.map((product, i) =>
              <p
                className='imported-goods__good'
                key={i}>
                <span className='imported-good__name'>
                  {t(`industry-card.cards.${id}.products.${product.name}`)}
                </span>
                <span className='imported-good__value'>
                  {product.value}
                </span>
              </p>
            )}
          </div>

        </div>
      </div>
    </div>
  )
}
