import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { YMaps, Map, Placemark } from 'react-yandex-maps'
import { PATH_MAP } from '../../routing'
import { CARDS, CardsInterface } from '../Projects-slider/constsnts'

import './investors-map.scss'

interface Prop {
  projects?: CardsInterface
  hideLinkToMap?: boolean
}

export const InvestorsMap = ({ projects = CARDS, hideLinkToMap: linkToMap }: Prop) => {
  const { t } = useTranslation()
  const [mapWidth, setMapWidth] = useState(0)

  useEffect(() => {
    const mapContainer = document.getElementById('investors-map__map')
    setMapWidth(mapContainer?.clientWidth || 250)
    window.addEventListener('resize', () => {
      setMapWidth(mapContainer?.clientWidth || 250)
    })
  })

  function onClickMark (e: any) {
    // console.log(e.originalEvent.target.properties._data.id)
  }

  const createProjectBalloonContent = (cityName: string) =>
    `<div id="balloon-content" class='balloon-content'>${projects.reduce((acc, project) => {
      // тут нужно вместо ссылки вставить кнопку и повесить обработчик на нее, но для этого нужен onopenballon
      if (cityName === project.city?.name) {
        const html = acc.concat(`
                <a href="/${project.id}"
                class="balloon-content__linck">
                ${t(`project-names.${project.projectName}`)}
                </a>
                `)
        acc = html
      }
      return acc
    }, '')
    }</div>`

  const createRegionBalloonContent = (regionName: string) =>
    `<div id="balloon-content" class='balloon-content'>${projects.reduce((acc, project) => {
      // тут нужно вместо ссылки вставить кнопку и повесить обработчик на нее, но для этого нужен onopenballon
      if (regionName === project.region.name) {
        const html = acc.concat(`
                <a href="/${project.id}"
                class="balloon-content__linck">
                ${t(`project-names.${project.projectName}`)}
                </a>
                `)
        acc = html
      }
      return acc
    }, '')
    }</div>`

  // const onOpenBallon = (ref: any) => {
  //     // console.log(ref);
  //     const balloonButtons = document.getElementsByClassName('balloon-content__button')
  //     console.log(balloonButtons);
  // }

  return (
    <section className='investors-map section'>
      <div className='investors-map__wrap container'>
        <h2 className='investors-map__title'>
          {t('investors-map.title')}
        </h2>

        <div className='investors-map__map-wrap'>
          <div className='investors-map__object-data'>

          </div>
          <div className='investors-map__map'
            id={'investors-map__map'}>
            <YMaps>
              <Map
                defaultState={{ center: [48.05, 67.05], zoom: 5 }}
                width={mapWidth - 1}
                height={600}>
                {projects.map((project, i) => {
                  if (project.city) {
                    return <Placemark
                      key={i}
                      geometry={project.city.geometry}
                      properties={
                        {
                          id: project.city.name,
                          balloonContent: createProjectBalloonContent(project.city.name)
                        }
                      }
                      modules={
                        ['geoObject.addon.balloon', 'geoObject.addon.hint']
                      }
                      onClick={(e: any) => onClickMark(e)}
                    // instanceRef={(ref: any) => {
                    //     ref && ref.balloon.open(onOpenBallon(ref));
                    // }}

                    //     events={

                    // }

                    // options={
                    //     {
                    //         // preset: 'islands#circleIcon'
                    //         iconLayout: 'default#image',
                    //         iconImageHref: logo,
                    //         iconImageSize: [48, 48],
                    //         iconImageOffset: [-24, -24],
                    //         iconContentOffset: [15, 15],
                    //         // iconContentLayout: 'MyIconContentLayout',
                    //     }
                    // }
                    />
                  } else {
                    return <></>
                  }
                }

                )}

                {projects.map((project, i) =>
                  <Placemark
                    key={i}
                    geometry={project.region.geometry}
                    properties={
                      {
                        iconContent: projects.reduce((acc, next) => {
                          if (next.region === project.region) {
                            acc = acc + 1
                          }
                          return acc
                        }, 0),
                        iconCaption: `${t(`projects_regions.${project.region.name}`)}`,
                        balloonContent: createRegionBalloonContent(project.region.name)
                      }
                    }
                    options={
                      {
                        preset: 'islands#circleIcon',
                        iconColor: 'red'
                      }
                    }
                    modules={
                      ['geoObject.addon.balloon', 'geoObject.addon.hint']
                    }
                  />
                )}
                {/* <ObjectManager
                                    objects={{
                                        openBalloonOnClick: false,
                                        preset: 'islands#greenDotIcon',
                                        onclick: () => console.log(777)
                                    }}
                                /> */}
              </Map>
            </YMaps>
          </div>
        </div>
        {
          linkToMap
            ? <></>
            : <Link
              className='investors-map__link-to-map react-link'
              to={PATH_MAP}>
              {t('investors-map.link-to-map')}
            </Link>
        }
      </div>
    </section>
  )
}
