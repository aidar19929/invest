import React from 'react'
import './Main.scss'

interface props {
  // eslint-disable-next-line no-undef
  children: JSX.Element | JSX.Element[]
}

export const Main = ({ children }: props) => (<main>{children}</main>)
