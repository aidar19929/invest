import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { MENU_TYPES } from '../../constants'

import './navs-bar.scss'

interface Prop {
  items: {
    name: string
    path: string
  }[]
  type?: string
}

export const NavsBar = ({ items, type }: Prop) => {
  const { t } = useTranslation()
  const { mobile } = MENU_TYPES

  return (
    <ul className={`main-header__navs-bar ${mobile === type ? 'mobile-navs' : ''}`}
    // style={{ display: visible ? 'none' : '' }}
    >
      {items.map((item, i) =>
        <li className="navs-item" key={i}>
          <Link
            className='react-link'
            to={item.path}>
            {t(`header.navs.${item.name}`)}
          </Link>
        </li>
      )}
    </ul>)
}
