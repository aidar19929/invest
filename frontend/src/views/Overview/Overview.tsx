import React from 'react'
import { useTranslation } from 'react-i18next'
import { useSelector } from 'react-redux'
import { RootState } from '../../redux/rootReducer'
import { chartData } from './constants'
import { Line } from 'react-chartjs-2'
import { DEVICES } from '../../constants'
import { getGradient } from '../../utils/getGradient'
import { MoreInformationButton } from '../../buttons/MoreInformationButton'

import { FIRST_BACKGROUND, MARK_COLOR, THIRD_BACKGROUND } from '../../style-constants'

import './index.scss'

export const Overview = () => {
  const { t } = useTranslation()
  const device = useSelector<RootState, string>(state => state.app.device)
  const { desktop, tablet, mobile } = DEVICES
  // fix responsiveness to constriction
  // нельзя сделать график меньше 320px поэтому на мобилке убираем паддинг контейнера
  const data = {
    labels: chartData.map((month) => month.month),
    datasets: [
      {
        label: '# of Votes',
        data: chartData.map((month) => month.firstData),
        fill: true,
        backgroundColor: (context: { chart: any; }) => {
          const chart = context.chart
          const { ctx, chartArea } = chart
          if (!chartArea) {
            return null
          }
          return getGradient(ctx, chartArea, ['rgba(45, 55, 72, 0.36)', 'rgba(45, 55, 72, 0)'])
        },
        borderColor: FIRST_BACKGROUND,
        tension: 0.7,
        radius: 0
      },

      {
        label: '# of No Votes',
        data: chartData.map((month) => month.secondData),
        fill: true,
        backgroundColor: (context: { chart: any; }) => {
          const chart = context.chart
          const { ctx, chartArea } = chart
          if (!chartArea) {
            return null
          }
          return getGradient(ctx, chartArea, ['rgba(79, 209, 197, 0.54)', 'rgba(79, 209, 197, 0)'])
        },
        borderColor: THIRD_BACKGROUND,
        tension: 0.6,
        radius: 0
      }
    ],

    options: {
      responsive: true,
      maintainAspectRatio: true
    }
  }

  const options = {
    type: 'line',
    plugins: {
      legend: {
        display: false
      }
    },
    scales: {
      y: {
        suggestedMin: 0,
        grid: {
        },
        ticks: {
          padding: device === desktop ? 28 : 12,
          stepSize: 100,
          color: MARK_COLOR
        }
      },
      x: {
        grid: {
          display: false
        },
        ticks: {
          padding: device === desktop ? 28 : 12,
          color: MARK_COLOR
        }
      }
    }
  }

  return (device !== tablet && device !== mobile)
    ? (
      <section className='overview section'>
        <div className='overview__wrap container'>
          <div className='overview__chart'>
            <h5 className='chart__chart-title'>
              {t('overview.chart-title')}
            </h5>
            <p className='chart__chart-paragraph'>
              <span>(+5) {t('overview.chart-paragraph-span')}</span> {t('overview.chart-paragraph')} 2020
            </p>

            <Line data={data} options={options} />
          </div>

          <div className='overview__text-wrap'>
            <h3 className='overview__title'>
              {t('overview.overview__title')}
            </h3>
            <p className='overview__text'>
              {t('overview.overview__text')}
            </p>

            <MoreInformationButton
              title={t('overview.more-information-button')}
            />
          </div>
        </div>
      </section>)
    : (
      <section className='overview section tablet-overview'>
        <div className='overview__wrap container'>
          <h3 className='overview__title'>
            {t('overview.overview__title')}
          </h3>
          <p className='overview__text'>
            {t('overview.overview__text')}
          </p>

          <div className='overview__chart-container'>
            <div className='overview__chart'>
              <h5 className='chart__chart-title'>
                {t('overview.chart-title')}
              </h5>
              <p className='chart__chart-paragraph'>
                <span>(+5) {t('overview.chart-paragraph-span')}</span> {t('overview.chart-paragraph')} 2020
              </p>

              <Line data={data} options={options} />
            </div>
            <MoreInformationButton
              title={t('overview.more-information-button')}
            />
          </div>
        </div>
      </section>)
}
