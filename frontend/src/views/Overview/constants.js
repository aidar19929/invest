export const chartData = [
    {
        month: 'Jan',
        firstData: 500,
        secondData: 155,
    },

    {
        month: 'Feb',
        firstData: 350,
        secondData: 300,
    },

    {
        month: 'Mar',
        firstData: 290,
        secondData: 450,
    },

    {
        month: 'Apr',
        firstData: 250,
        secondData: 400,
    },

    {
        month: 'May',
        firstData: 290,
        secondData: 430,
    },

    {
        month: 'Jun',
        firstData: 250,
        secondData: 450,
    },

    {
        month: 'Jul',
        firstData: 210,
        secondData: 300,
    },

    {
        month: 'Aug',
        firstData: 200,
        secondData: 250,
    },

    {
        month: 'Sep',
        firstData: 190,
        secondData: 300,
    },

    {
        month: 'Oct',
        firstData: 250,
        secondData: 350,
    },

    {
        month: 'Nov',
        firstData: 280,
        secondData: 450,
    },

    {
        month: 'Dec',
        firstData: 300,
        secondData: 400,
    },
]