import React from 'react'
import { useTranslation } from 'react-i18next'
import { FilterButton } from '../../buttons/FilterButton'
import { ProjectsToGridButton } from '../../buttons/ProjectsToGridButton'
import { ProjectsToListButton } from '../../buttons/ProjectsToListButton'
import { ToMapButton } from '../../buttons/ToMapButton'
import { FilterForm } from '../Filter-form'

import './page-controller.scss'

interface Props {
  onClickToGrid?: Function
  onClickToList?: Function
  setProjects?: Function
  toMapHidden?: boolean
}

export const PageController = ({
  onClickToGrid,
  onClickToList,
  setProjects,
  toMapHidden
}: Props) => {
  const { t } = useTranslation()
  const [open, setOpen] = React.useState(false)

  const openModal = () => {
    setOpen(true)
  }

  function returnFilteredProjects (project: any) {
    if (setProjects) {
      setProjects(project)
    }
    setOpen(false)
  }

  return (
    <section className='page-controller section'>
      <div className='page-controller__wrap container'>
        {
          setProjects
            ? <FilterButton
              title={t('page-controller.filter')}
              onClick={openModal}
            />
            : <></>
        }
        {
          onClickToGrid
            ? <ProjectsToGridButton
              onClick={onClickToGrid}
              title={t('page-controller.to-grid-button')}
            />
            : <></>
        }
        {
          onClickToList
            ? <ProjectsToListButton
              onClick={onClickToList}
              title={t('page-controller.to-list-button')}
            />
            : <></>
        }
        {
          toMapHidden
            ? <></>
            : <ToMapButton
              title={t('page-controller.to-map-button')}
            />
        }

        <FilterForm
          open={open}
          returnFilteredProjects={returnFilteredProjects}
          closeModal={() => setOpen(false)}
        />
      </div>
    </section>
  )
}
