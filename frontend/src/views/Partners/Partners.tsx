import React from 'react'
import { useTranslation } from 'react-i18next'
import { Carousel } from '../Carousel'

import './partners.scss'

export const Partners = () => {
  const { t } = useTranslation()

  return (
    <section className='partners section'>
      <h2 className='partners__title'>
        {t('partners.title')}
      </h2>

      <Carousel />
    </section>
  )
}
