import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import cardBackgroundImg from '../../assets/images/card-background.png'

import './project-card.scss'

interface Prop {
  layautIsList?: boolean,
  allInformation?: boolean,
  project: {
    stage: string,
    projectName: string,
    manufacturedProducts: string[],
    region: {
      name: string
      geometry: number[]
    },
    city: {
      name: string
      geometry: number[]
    } | null,
    id: string,
    themeIswhite: boolean,
    сommissioningYear: number,
    projectCost: number,
    projectCapacity: number,
  }
}

export const ProjectCard = ({ project, layautIsList, allInformation }: Prop) => {
  const { t } = useTranslation()
  const {
    themeIswhite,
    сommissioningYear,
    id,
    stage,
    projectCost,
    projectCapacity,
    projectName,
    manufacturedProducts,
    city,
    region
  } = project

  return (
    <div className='project-card'>
      <div className={`project-card__wrap ${themeIswhite ? 'white-style' : ''} ${layautIsList ? 'list-layaut' : ''}`}>
        <img
          className='card__card-background'
          src={cardBackgroundImg}
          alt='card background'
        />

        <p className='card__stage'>
          {t(`project-stages.${stage}`)}
        </p>

        <h4 className='card__project-name'>
          {t(`project-names.${projectName}`)}
        </h4>

        <ul className={`card__main-project-information ${themeIswhite ? 'information__white-style' : ''}`}>
          <li className='card__manufactured-products'>
            <span className='manufactured-products__label'>
              {t('project-card.manufactured-products__label')}
            </span>
            <p className='manufactured-products__products'>
              {manufacturedProducts.map((product, i) =>
                <span key={i}>
                  {t(`projects_manufactured_products.${product}`)}
                </span>)}
            </p>
          </li>

          <li className='card__project-address'>
            <span className='project-address__label'>
              {t('project-card.project-address__label')}
            </span>
            <span className='project-address__address' >
              <span className='project-address__region'>
                {`${t(`projects_regions.${region.name}`)} `}
              </span>

              <span className='project-address__city'>
                {city ? t(`projects_cityes.${city.name}`) : ''}
              </span>
            </span>
          </li>

          <li className='card__сommissioning-year'>
            <span className='сommissioning-year__label'>
              {t('project-card.сommissioning-year__label')}
            </span>
            <span className='сommissioning-year__year' >
              {сommissioningYear}
            </span>
          </li>

          <li className='card__project-cost'>
            <span className='project-cost__label'>
              {t('project-card.project-cost__label')}
            </span>
            <p className='project-cost__cost' >
              <span className='project-cost__value'>
                {`${projectCost} `}
              </span>
              <span className='project-cost__measure'>
                {t(`project-card.cards.${id}.project-cost__measure`)}
              </span>
            </p>
          </li>

          <li className='card__project-capacity'>
            <span className='project-capacity__label'>
              {t('project-card.project-capacity__label')}
            </span>
            <p className='project-capacity__capacity' >
              <span className='project-capacity__value'>
                {`${projectCapacity} `}
              </span>
              <span className='project-capacity__measure'>
                {t(`project-card.cards.${id}.project-capacity__measure`)}
              </span>
            </p>
          </li>
        </ul>

        <Link
          className='card__link-to-project'
          to={`/${id}`}
          style={{ display: `${!allInformation ? '' : 'none'}` }}>
          {t('project-card.card__link-to-project')}
        </Link>

        <div className='card__all-information'
          style={{ display: `${allInformation ? '' : 'none'}` }}>
          <h1>ALL INFORMATION WILL BE HERE</h1>
        </div>
      </div>
    </div>
  )
}
