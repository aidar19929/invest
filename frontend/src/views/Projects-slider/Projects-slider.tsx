import React, { useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { DEVICES } from '../../constants'
import { useSelector } from 'react-redux'
import { RootState } from '../../redux/rootReducer'
// documentation https://nickpiscitelli.github.io/Glider.js/
import Glider from 'glider-js'
import { PATH_PROJECTS } from '../../routing'
import { CARDS } from './constsnts'
import { ProjectCard } from '../Project-card'
import { Link } from 'react-router-dom'

import './projects-slider.scss'

export const ProjectsSlider = () => {
  const { mobile, tablet } = DEVICES
  const { t } = useTranslation()
  const device = useSelector<RootState, string>(state => state.app.device)

  useEffect(() => {
    const glide = document.getElementById('projects-slider__cards') || document.createElement('div')

    const glider = new Glider(glide, {
      slidesToShow: 'auto',
      itemWidth: 360,
      dots: '.projects-slider__dots',
      draggable: true,
      scrollLock: true
    })

    if (device === mobile || device === tablet) {
      glider.setOption({
        slidesToShow: 1
      })
    }

    if (device !== mobile && device !== tablet) {
      glider.setOption({
        slidesToShow: 'auto'
      })
    }

    glider.refresh(true)
  }, [device])

  return (
    <section className='projects-slider'>
      <div className='projects-slider__wrap container'>
        <h2 className='projects-slider__title'>{t('projects-slider.title')}</h2>
        <div className='projects-slider__cards' id={'projects-slider__cards'}>
          {
            CARDS.map((project, i) => (
              <ProjectCard
                key={i}
                project={project}
              />
            ))
          }
        </div>
        <div role="dots" className="projects-slider__dots"></div>

        <Link
          className='projects-slider__link-to-projects react-link'
          to={PATH_PROJECTS}>
          {t('projects-slider.link-to-projects')}
        </Link>
      </div>
    </section>
  )
}
