import {
  PROJECT_STAGES,
  PROJECT_NAMES,
  PROJECTS_MANUFACTURED_PRODUCTS,
  PROJECTS_REGIONS,
  PROJECTS_CITYES
} from '../../constants'

const {
  battery,
  carAxles,
  cable,
  ceramics,
  electricalProducts
} = PROJECTS_MANUFACTURED_PRODUCTS

const {
  westKazakhstan,
  kostanayRegion,
  karagandaRegion,
  akmolaRegion
} = PROJECTS_REGIONS

interface Card {
  id: string,
  stage: string,
  projectName: string,
  manufacturedProducts: string[],
  themeIswhite: boolean,
  сommissioningYear: number,
  projectCost: number,
  region: {
    name: string
    geometry: number[]
  },
  city: {
    name: string
    geometry: number[]
  } | null,
  projectCapacity: number
}

export interface CardsInterface extends Array<Card> { }

export const CARDS: CardsInterface = [
  {
    id: '0',
    stage: PROJECT_STAGES.perspective,
    projectName: PROJECT_NAMES.kainarAcc,
    manufacturedProducts: [battery],
    themeIswhite: true,
    сommissioningYear: 2022,
    projectCost: 20,
    region: westKazakhstan,
    city: null,
    projectCapacity: 1.2
  },
  {
    id: '1',
    stage: PROJECT_STAGES.implemented,
    projectName: PROJECT_NAMES.kamLitKZ,
    manufacturedProducts: [carAxles],
    themeIswhite: true,
    сommissioningYear: 2025,
    projectCost: 55,
    region: kostanayRegion,
    city: null,
    projectCapacity: 16.6
  },
  {
    id: '2',
    stage: PROJECT_STAGES.perspective,
    projectName: PROJECT_NAMES.centralAsiaCable,
    manufacturedProducts: [cable],
    themeIswhite: true,
    сommissioningYear: 2023,
    projectCost: 13.7,
    region: karagandaRegion,
    city: null,
    projectCapacity: 15.6
  },
  {
    id: '3',
    stage: PROJECT_STAGES.implemented,
    projectName: PROJECT_NAMES.mabetexGroup,
    manufacturedProducts: [ceramics],
    themeIswhite: true,
    сommissioningYear: 2023,
    projectCost: 60.9,
    region: akmolaRegion,
    city: PROJECTS_CITYES.astana,
    projectCapacity: 570
  },
  {
    id: '4',
    stage: PROJECT_STAGES.implemented,
    projectName: PROJECT_NAMES.bolashakElectric,
    manufacturedProducts: [electricalProducts],
    themeIswhite: true,
    сommissioningYear: 2022,
    projectCost: 3,
    region: akmolaRegion,
    city: PROJECTS_CITYES.astana,
    projectCapacity: 10.8
  }
]
