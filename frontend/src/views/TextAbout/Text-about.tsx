import React from 'react'
import { useTranslation } from 'react-i18next'

import './text-about.scss'

export const TextAbout = () => {
  const { t } = useTranslation()

  return (
    <section className='text-about section'>
      <div className='text-about__wrap container'>
        <h2 className='text-about__title'>
          {t('text-about.title')}
        </h2>
        <div className='text-about-texts'>
          <p className='text-about__first-text text'>
            {t('text-about.first-text')}
          </p>

          <p className='text-about__second-text text'>
            {t('text-about.second-text')}
          </p>
        </div>
        <p className='text-about__copyright'>
          <span className='text-about__first-text text'>
            {`© ${t('text-about.copyright')}`}
          </span>
        </p>
      </div>
    </section>
  )
}
