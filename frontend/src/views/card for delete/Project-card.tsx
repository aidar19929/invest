import React from 'react'
import { useTranslation } from 'react-i18next'
import { CardItem } from '../Card-item/Card-item'

import whiteStar from '../../../assets/images/Star-white.svg'
import blackStar from '../../../assets/images/black-star.png'
import cardBackgroundImg from '../../../assets/images/card-background.png'

import './project-card.scss'

interface Prop {
  title: string
  items: Array<{
    color: string
    percent: string
    text: string
  }>
  themeIswhite: boolean
}

export const ProjectCard = ({ title, items, themeIswhite }: Prop) => {
  const { t } = useTranslation()

  const cardBackground = themeIswhite ? '#FFFFFF' : '#353535'
  const movementBackground = themeIswhite ? 'linear-gradient(234.53deg, #2CF2FF -33%, rgba(21, 184, 194, 0) 94.34%)' : '#368B85'
  const color = themeIswhite ? '' : '#FFFFFF'
  const star = themeIswhite ? blackStar : whiteStar

  return (
    <div
      className='card'
      style={{
        background: `${cardBackground}`,
        color: `${color}`
      }}>

      <img
        className='card__card-background'
        src={cardBackgroundImg}
        alt='card background'
      />

      <ul className='card__rating'>
        <li className='rating__star-wrap'>
          <img
            className='rating__star-img'
            src={star}
            alt='rating disabled star' />
        </li>

        <li className='rating__star-wrap'>
          <img
            className='rating__star-img'
            src={star}
            alt='rating disabled star'
          />
        </li>

        <li className='rating__star-wrap'>
          <img
            className='rating__star-img'
            src={star}
            alt='rating disabled star'
          />
        </li>

        <li className='rating__star-wrap'>
          <img
            className='rating__star-img'
            src={star}
            alt='rating disabled star'
          />
        </li>

        <li className='rating__star-wrap'>
          <img
            className='rating__star-img'
            src={star}
            alt='rating disabled star'
          />
        </li>
      </ul>
      <h4 className='cart__tite'>
        {title}
      </h4>
      <div
        className='card__movement-wrap'
        style={{ background: `${movementBackground}` }}>
        <h4 className='card__movement'>
          +9.9%
        </h4>
      </div>
      {
        items.map((item, i) => (
          <CardItem
            key={i}
            color={item.color}
            percent={item.percent}
            text={t(`strategy.cards.${item.text}`)}
          />
        ))
      }
    </div>
  )
}
